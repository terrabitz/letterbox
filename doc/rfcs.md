# Important RFCs

- [Email format](https://tools.ietf.org/html/rfc5322)
- [Multipart MIME](https://tools.ietf.org/html/rfc2045)
- [Multipart MIME 2](https://tools.ietf.org/html/rfc2046)
- [DKIM](https://tools.ietf.org/html/rfc6376)
- [SPF](https://tools.ietf.org/html/rfc7208)
- [DMARC](https://tools.ietf.org/html/rfc7489)
- [S/MIME](https://tools.ietf.org/html/rfc5652)
- [PGP-MIME](https://tools.ietf.org/html/rfc3156)
- [TLS Extensions](https://tools.ietf.org/html/rfc6066)
- [Implicit TLS](https://tools.ietf.org/html/rfc8314)
- [REQUIRETLS](https://tools.ietf.org/html/draft-ietf-uta-smtp-require-tls-09)
- [MTA-STS](https://tools.ietf.org/html/rfc8461)

## Other articles

- [Email Security Guide](https://dmarcly.com/blog/how-to-implement-dmarc-dkim-spf-to-stop-email-spoofing-phishing-the-definitive-guide)
