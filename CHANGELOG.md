# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.7...v0.1.8) (2021-01-17)


### Bug Fixes

* Fix variadic field passing ([a4dd6a1](https://gitlab.com/hackandsla.sh/letterbox/commit/a4dd6a182ec9d7ce59a25cf909b7346b7d82c644))
* Tune ane improve logging consistency ([ef8c6bd](https://gitlab.com/hackandsla.sh/letterbox/commit/ef8c6bdfb6d5ba05ebca98e31a97bb43bc977cab))

### [0.1.7](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.6...v0.1.7) (2021-01-17)


### Bug Fixes

* Parse encoded messages from multi-part emails ([39d1604](https://gitlab.com/hackandsla.sh/letterbox/commit/39d1604e1ce834b7d5152303632e2d787b9e3cfc))

### [0.1.6](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.5...v0.1.6) (2020-11-30)


### Features

* Add function to disable mailboxes via an external ID ([5356653](https://gitlab.com/hackandsla.sh/letterbox/commit/5356653a500f245f0805f9d1c383f4e521096237))
* include disable link in forwarded email ([34f1c75](https://gitlab.com/hackandsla.sh/letterbox/commit/34f1c75012457f3e29b6cd942a856bd00d4393ce))


### Bug Fixes

* Add banner config options ([06cfb52](https://gitlab.com/hackandsla.sh/letterbox/commit/06cfb528ffd8ff7a0b4476bdfce4099fe110c525))
* Exclude external ID from JSON output ([cfab95b](https://gitlab.com/hackandsla.sh/letterbox/commit/cfab95b711e499d8acaecc3bb6a4dc88201148a7))
* Make SMTP interfaces private ([06d4408](https://gitlab.com/hackandsla.sh/letterbox/commit/06d440815feedc9861865a8d58ed1525296e97c9))

### [0.1.3](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.2...v0.1.3) (2020-11-19)

### [0.1.2](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.0...v0.1.2) (2020-11-18)

### [0.1.1](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.1.0...v0.1.1) (2020-10-19)

### [0.0.21](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.20...v0.0.21) (2020-10-19)


### Bug Fixes

* Remove references to custom error types ([f85089c](https://gitlab.com/hackandsla.sh/letterbox/commit/f85089c0fdb0c36aec8ba9a112bd0870a1f4ee46))
* Unexport badactor service field ([bdd39f4](https://gitlab.com/hackandsla.sh/letterbox/commit/bdd39f4299ffa03ffd73afcbde3acbd80abe8820))

### [0.0.20](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.19...v0.0.20) (2020-09-17)


### Features

* Add user-specific crypto keys ([3754b81](https://gitlab.com/hackandsla.sh/letterbox/commit/3754b81e98b5e15d99f3f74e3ebc5903fac8fe15))
* Allow for transparent key passphrase rotation ([bcff8e1](https://gitlab.com/hackandsla.sh/letterbox/commit/bcff8e121cc62377795d99ed8ee4ef6fa6253c63))
* Allow updating user key ([3643580](https://gitlab.com/hackandsla.sh/letterbox/commit/364358084840963af0d21189a9f70cc830493699))
* Encrypt message via a user key ([fdffd7f](https://gitlab.com/hackandsla.sh/letterbox/commit/fdffd7f1416875baa7272b43c7da2d05299a0cde))

### [0.0.19](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.18...v0.0.19) (2020-07-16)

### [0.0.18](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.17...v0.0.18) (2020-06-29)


### Features

* Add ability to forward proxied emails ([bf5165a](https://gitlab.com/hackandsla.sh/letterbox/commit/bf5165a5b6ec75570e09dbf9c25006158e3c7f51))
* Add endpoint to update users ([b1967a2](https://gitlab.com/hackandsla.sh/letterbox/commit/b1967a26d175c89675a3c37e7e2075a9cd2ca4ae))

### [0.0.17](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.16...v0.0.17) (2020-06-02)


### Features

* Add ability to configure source domain whitelist ([04d01ba](https://gitlab.com/hackandsla.sh/letterbox/commit/04d01ba16269042ec28695c50f77f869742c97c9))

### [0.0.16](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.15...v0.0.16) (2020-06-01)


### Features

* Add domain statistics collection ([048ca29](https://gitlab.com/hackandsla.sh/letterbox/commit/048ca29e368dcb22cea12446e76d7d5ba98862c6))

### [0.0.15](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.14...v0.0.15) (2020-06-01)


### Features

* Add endpoints to delete mailboxes and domains ([8a15fe2](https://gitlab.com/hackandsla.sh/letterbox/commit/8a15fe207ba258e14f53aad7969d731dea929dbc))
* Add optional expiry field to mailboxesMailboxes can now be set with an optional "expires" value. If set, themailbox will be removed by the cleanup service (which runs by defaultonce per minute) if it has expired.Signed-off-by: Trevor Taubitz <terrabitz@protonmail.com> ([c95e96f](https://gitlab.com/hackandsla.sh/letterbox/commit/c95e96fd0956269e9eb9048b97048ee538d4c29a))

### [0.0.14](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.13...v0.0.14) (2020-05-31)


### Features

* Add body parsing ([67d7ca4](https://gitlab.com/hackandsla.sh/letterbox/commit/67d7ca4cadb0b1e7aa6f74e411834f900d43df05))
* Allow setting max email size ([ff411a1](https://gitlab.com/hackandsla.sh/letterbox/commit/ff411a171c0f172cac680d235fff7e2acaf70d88))

### [0.0.13](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.12...v0.0.13) (2020-05-26)


### Features

* Limit requests for IPs with too many failed logins ([010b2e6](https://gitlab.com/hackandsla.sh/letterbox/commit/010b2e671c61cd3c8e182b9dcccb5b78a14b98de))
* Limit requests from IPs that fail too many credential lookups ([6495799](https://gitlab.com/hackandsla.sh/letterbox/commit/64957993a8308f948af40379b0e17274cd32aff8))

### [0.0.12](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.11...v0.0.12) (2020-05-26)


### Bug Fixes

* Add workaround for Windows terminal error ([619d820](https://gitlab.com/hackandsla.sh/letterbox/commit/619d820128d1a634b5696159926b3e95e03fc3db))

### [0.0.11](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.10...v0.0.11) (2020-05-26)


### Features

* Add DKIM alignment checks ([8a2e23c](https://gitlab.com/hackandsla.sh/letterbox/commit/8a2e23cb05199ae506e68a0a6ff6f7e0cb07fa66))
* Add error logging to SMTP server ([c610e08](https://gitlab.com/hackandsla.sh/letterbox/commit/c610e0886aeec4042c0abc1a4ed10604ff56f0ea))
* Add init subcommand to add initial admin ([cafb609](https://gitlab.com/hackandsla.sh/letterbox/commit/cafb609f0f2a649bf52a826e8a39f8e86199fb2d))
* Add SPF alignment checks ([da21035](https://gitlab.com/hackandsla.sh/letterbox/commit/da2103578dc923bac37cf1d64c5e0f12e6f22439))


### Bug Fixes

* Fix SPF logging ([a93b477](https://gitlab.com/hackandsla.sh/letterbox/commit/a93b477d809979e7cd7084b78d8f0473e1f1d6ef))
* Suppress contextual SPF information on pass ([9e97e51](https://gitlab.com/hackandsla.sh/letterbox/commit/9e97e517f799767bd386f64dc0034fa23c5f1197))

### [0.0.10](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.9...v0.0.10) (2020-05-25)


### Features

* Add DKIM validation ([64bd51a](https://gitlab.com/hackandsla.sh/letterbox/commit/64bd51a0e0998212c95774c2551443d5a7aeafd6))
* Add SPF validation ([722bc8f](https://gitlab.com/hackandsla.sh/letterbox/commit/722bc8f36fbc498b8f2db90275685b75e8c402ed))


### Bug Fixes

* Fix domain-scoping for mailbox retrieval ([d080030](https://gitlab.com/hackandsla.sh/letterbox/commit/d080030fb0fd97287fa78a5e525d81e742bd32a9))

### [0.0.9](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.8...v0.0.9) (2020-05-24)


### Features

* Return JSON objects on error ([65864f9](https://gitlab.com/hackandsla.sh/letterbox/commit/65864f9335cdb403550fd1c376a7655fc46a5fdf))


### Bug Fixes

* Remove go-spew debug call ([1d0a379](https://gitlab.com/hackandsla.sh/letterbox/commit/1d0a379e47e5dfecbca31258c6edbdd9ca7b9464))

### [0.0.8](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.7...v0.0.8) (2020-05-21)


### Features

* Add ability to designate a catch-all mailbox ([2e08ba2](https://gitlab.com/hackandsla.sh/letterbox/commit/2e08ba279c8bdfc33522a6333e77d58b81742007)), closes [#29](https://gitlab.com/hackandsla.sh/letterbox/issues/29)
* Parse SMTP messages ([8721fbc](https://gitlab.com/hackandsla.sh/letterbox/commit/8721fbc24cd7566e87f5b7933fe9f3c44f69cabe)), closes [#27](https://gitlab.com/hackandsla.sh/letterbox/issues/27)

### [0.0.7](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.6...v0.0.7) (2020-05-18)


### Features

* Add authentication to core API ([ed680c1](https://gitlab.com/hackandsla.sh/letterbox/commit/ed680c18bcd7476e1a375463b2c5596decbfd8ad))
* Add initial admin creator endpoint ([9d38e2b](https://gitlab.com/hackandsla.sh/letterbox/commit/9d38e2b9e1058b1a89892ca56526d00d4511d408))
* Allow initial admin account to be created via env configuration ([dbe2ad2](https://gitlab.com/hackandsla.sh/letterbox/commit/dbe2ad2fb5019bf2c6ace73207a0850a3917325f))

### [0.0.6](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.5...v0.0.6) (2020-05-14)


### Bug Fixes

* Add additional checks for invalid email addresses ([476583f](https://gitlab.com/hackandsla.sh/letterbox/commit/476583f4cfbc570c62bd73ae26232e130d71b8c6))
* Add check for mailboxes with the same name within the current domain ([bac29e3](https://gitlab.com/hackandsla.sh/letterbox/commit/bac29e34cd75bff18ad5fabea55d78e0bb2ba2ed))
* Ignore logger sync error ([b8a43da](https://gitlab.com/hackandsla.sh/letterbox/commit/b8a43dab11eda589cdb6e68bb1917d112c3cea94))
* Wait for both SMTP and HTTP servers to quit ([8e5a536](https://gitlab.com/hackandsla.sh/letterbox/commit/8e5a53644a35b233fe282eb3460b4679599229bd))

### [0.0.5](https://gitlab.com/hackandsla.sh/letterbox/compare/v0.0.4...v0.0.5) (2020-05-04)

### Features

* Add backend database ([c69da4b](https://gitlab.com/hackandsla.sh/letterbox/commit/c69da4bc2db080acd32d59c485a76e56ce0c72f7))
* Add domains model ([8b98438](https://gitlab.com/hackandsla.sh/letterbox/commit/8b98438e29be3aead8ab2d6b48b7bdab255ad9d3))
* Add endpoint to retrieve messages ([33cb019](https://gitlab.com/hackandsla.sh/letterbox/commit/33cb019ad35188cda88076e371b8cc2d5db5f879))
* Add endpoint to retrieve single mailbox ([c047ca2](https://gitlab.com/hackandsla.sh/letterbox/commit/c047ca25f5484d2641f34c94442caa03277909bb))
* Add initial mailbox implementation ([3241ea8](https://gitlab.com/hackandsla.sh/letterbox/commit/3241ea810f2c14112b896bd8007767bd00461f47))
* Add SMTP server ([b8ed688](https://gitlab.com/hackandsla.sh/letterbox/commit/b8ed6887a03ed71729f4a0f81c6ff419fbf23c16))


### Bug Fixes

* Use domain name as primary key ([ffb909e](https://gitlab.com/hackandsla.sh/letterbox/commit/ffb909ed6879724efe924789ed98e80d32eda91b))
