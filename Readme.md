![letterbox logo](media/letterbox_logo.png)

# letterbox

A privacy-focused, self-hostable email proxy and temporary email solution

## Installation

To get a pre-compiled binary for your architecture, just head over to the [releases](https://gitlab.com/hackandsla.sh/letterbox/-/releases) page.

If you would like to compile it yourself:

```bash
go get gitlab.com/hackandsla.sh/letterbox
```

## Usage

```help
$ ./letterbox -h
letterbox helps do stuff

Usage:
  letterbox [flags]
  letterbox [command]

Available Commands:
  completion  Generate shell completions
  docs        Generate docs for this package
  help        Help about any command
  init        Initialize the initial admin user
  smtp-cli    Send a SMTP message

Flags:
      --cleanup-interval duration       The time between expired mailbox cleanup (default 1m0s)
      --config string                   config file
      --db-database string              Specify the database to use (default "letterbox")
      --db-host string                  Specify the database host to use
      --db-insecure                     If specified, will not use SSL to connect to the database
      --db-password string              Specify database password. ONLY USE THE ON THE CLI FOR TESTING
      --db-port int                     Specify the database port to use
      --db-user string                  Specify database user (default "letterbox")
      --development                     Enable development mode
  -h, --help                            help for letterbox
      --init-password string            The initial admin password. If specified, letterbox will attempt to create the initial admin account. Otherwise, an admin account will have to be specified manually.
      --init-username string            The initial admin username
      --jail-time duration              The amount of time an IP gets banned for (default 10m0s)
  -l, --log-file string                 File to log to
      --max-auth-attempts int           The allowed number of auth attempts before the IP is banned (default 10)
      --max-shutdown-seconds int        The max time (in seconds) to wait for the HTTP server to shut down (default 30)
  -p, --port int                        Port to run HTTP server on (default 8080)
      --smtp-hostname string            The hostname to use on the SMTP server
      --smtp-max-size string            The maximum email size to accept (default "10MB")
      --smtp-outbound-host string       The host to use to send proxied emails
      --smtp-outbound-password string   The password to authenticate to the outbound SMTP server with
      --smtp-outbound-port int          The port to use to send proxied emails (default 587)
      --smtp-outbound-username string   The username to authenticate to the outbound SMTP server with
      --smtp-port int                   Specify the SMTP port to use (default 587)
      --smtp-tls-cert string            The TLS certificate string to use
      --smtp-tls-certfile string        The TLS certificate file to use
      --smtp-tls-key string             The TLS key string to use
      --smtp-tls-keyfile string         The TLS key file to use
      --smtp-tls-port int               The port to use for the implicit TLS server (see RFC8314) (default 465)
  -v, --verbose                         Run with verbose logs

Use "letterbox [command] --help" for more information about a command.
```

## Docker usage

To run the tool with Docker, just run the latest version of the `hackandslash/letterbox` image:

```bash
docker run \
    hackandslash/letterbox
```

## Docker-Compose Usage

To run the server as simply as possible:

1. Copy the contents of [docker-compose-simple.yaml](doc/docker-compose-simple.yaml) into a file called `docker-compose.yaml`.
2. Change `${SECURE_PASSWORD}` to a securely-generated random password
3. Run `docker-compose up -d`

## About Me

Trevor Taubitz – [@terrabitz](https://twitter.com/terrabitz)

Distributed under the AGPL license. See `LICENSE` for more information.

[https://gitlab.com/terrabitz/](https://gitlab.com/terrabitz)
