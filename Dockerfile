FROM golang:1.15-buster as builder

# Create the user and group files to run unprivileged
RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:65534:' > /user/group
RUN apt-get update && \
    apt-get install -y gcc git ca-certificates tzdata

FROM scratch as final
ENV HTTP_PORT 8080
ENV SMTP_PORT 5587
ENV SMTP_TLS_PORT 4465
ENV PATH "/"
LABEL author="Trevor Taubitz"
# Import the timezone files
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
# Import the user and group files
COPY --from=builder /user/group /user/passwd /etc/
# Import the CA certs
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
WORKDIR /
# Run as unprivileged
USER nobody:nobody

ARG BIN
# ENV BIN=$BIN
COPY $BIN "/letterbox"
ENTRYPOINT ["/letterbox"]