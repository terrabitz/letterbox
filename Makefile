APP = ./letterbox

build:
	go build -o $(APP)
	sudo setcap 'cap_net_bind_service=+ep' $(APP)

start-dev-server:
	CompileDaemon -command="./letterbox" -build="make build"

dev:
	docker-compose up -d
	docker-compose logs -f webapp 

dev-refresh:
	docker-compose down

build-ui:
	swagger generate spec -o api/swagger.yaml
	redoc-cli bundle -o static/doc/index.html api/swagger.yaml
	pkger

ca:
	cfssl gencert -initca test/cert/ca-csr.json | cfssljson -bare test/cert/ca -

cert: ca
	cfssl gencert -ca=test/cert/ca.pem -ca-key=test/cert/ca-key.pem -config=test/cert/ca-config.json -profile=server test/cert/server.json | cfssljson -bare test/cert/server

build-snapshot:
	docker run --rm --privileged -v $(shell pwd):/go/src/gitlab.com/hackandsla.sh/letterbox -v /var/run/docker.sock:/var/run/docker.sock -w /go/src/gitlab.com/hackandsla.sh/letterbox -e GITLAB_TOKEN goreleaser/goreleaser:v0.132.1 release --debug --snapshot --skip-publish --rm-dist

remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|master)" | xargs git branch -d

gen:
	go generate ./...

test: gen
	go test ./...

lint:
	golangci-lint run

tidy: 
	./build/tidy.sh

pull-master:
	git checkout master
	git pull

release: pull-master test lint tidy
	standard-version

.PHONY: build