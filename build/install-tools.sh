#!/bin/bash

# This script installs our go-based development binaries as a workaround for https://github.com/golang/go/issues/40276

pushd $(mktemp -d)
go mod init tools

go get github.com/markbates/pkger/cmd/pkger@v0.17.1
go get github.com/go-swagger/go-swagger/cmd/swagger@v0.25.0
go get github.com/abice/go-enum@v0.2.3

if [ "$1" == "dev" ]; then
go get github.com/cloudflare/cfssl/cmd/cfssl@v1.5.0
go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.32.1
go get github.com/githubnemo/CompileDaemon@v1.2.1
fi

popd