package server

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"

	"gitlab.com/hackandsla.sh/letterbox/internal/badactorservice"
	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/cleanup"
	"gitlab.com/hackandsla.sh/letterbox/internal/db"
	"gitlab.com/hackandsla.sh/letterbox/internal/httpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger/zap"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver"
)

// Config contains all the configuration options for the server.
// Everything here should be operator-accessible.
type Config struct {
	Development bool                    `json:"development,omitempty"`
	HTTP        *httpserver.Config      `json:"http,omitempty"`
	DB          *db.Config              `json:"db,omitempty"`
	SMTP        *smtpserver.Config      `json:"smtp,omitempty"`
	Log         *zap.Config             `json:"log,omitempty"`
	Init        *InitConfig             `json:"init,omitempty"`
	BadActor    *badactorservice.Config `json:"badactor,omitempty"`
	Cleanup     *cleanup.Config         `json:"cleanup,omitempty"`
	Banner      *banner.Config          `json:"banner,omitempty"`
}

// Validate validates the the server config and all its sub-server configs are
// valid.
func (s *Config) Validate() error {
	return validation.ValidateStruct(s,
		validation.Field(&s.HTTP),
		validation.Field(&s.DB),
		validation.Field(&s.SMTP),
		validation.Field(&s.Log),
		validation.Field(&s.BadActor),
		validation.Field(&s.Cleanup),
		validation.Field(&s.Banner),
	)
}

// InitConfig defines the config needed to bootstrap the initial admin user.
type InitConfig struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}
