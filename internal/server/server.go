package server

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/alexedwards/argon2id"
	_ "github.com/jinzhu/gorm/dialects/postgres" // Use Postgres GORM dialect
	"github.com/prometheus/common/log"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/cleanup"
	"gitlab.com/hackandsla.sh/letterbox/internal/db"
	"gitlab.com/hackandsla.sh/letterbox/internal/httpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger/zap"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/dkim"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpserver/spf"
)

// Server describes the server configuration.
type Server struct {
	Config             Config
	Info               *letterbox.Info
	Logger             LoggerService
	DB                 letterbox.Datastore
	HTTP               *httpserver.HTTPService
	SMTP               *smtpserver.SMTPService
	Cleanup            *cleanup.Cleanup
	RegisteredServices []Service
	Hasher             letterbox.ArgonHasher
	Banner             *banner.Builder
}

func New(config *Config, i *letterbox.Info) *Server {
	return &Server{
		Config: *config,
		Info:   i,
	}
}

// Start initializes a server instance and start the server.
//nolint:funlen // This is just startup wiring code
func (srv *Server) Start() error {
	var err error

	srv.Hasher = letterbox.ArgonHasher{
		Params: *argon2id.DefaultParams,
	}

	srv.Banner, err = banner.NewBuilder(srv.Config.Banner)
	if err != nil {
		return fmt.Errorf("couldn't initialize banner builder: %w", err)
	}

	srv.Logger, err = zap.New(srv.Config.Log)
	if err != nil {
		log.Errorf("could not initialize logger service: %s", err)
		return nil
	}

	srv.RegisterService(srv.Logger)

	sqlDB := &db.SQLDatastore{
		Config: *srv.Config.DB,
	}
	srv.DB = sqlDB
	srv.RegisterService(sqlDB)

	srv.HTTP = &httpserver.HTTPService{
		Config:     *srv.Config.HTTP,
		DB:         srv.DB,
		Logger:     srv.Logger,
		ServerInfo: srv.Info,
		Hasher:     srv.Hasher,
	}
	srv.RegisterService(srv.HTTP)

	domainAlignmentCheck := &letterbox.FlexibleDomainCheck{}

	dkimCheck := &dkim.Client{
		DomainAlignment: domainAlignmentCheck,
	}

	spfCheck := &spf.Client{
		DomainAlignment: domainAlignmentCheck,
	}

	srv.SMTP = &smtpserver.SMTPService{
		Config: *srv.Config.SMTP,
		DB:     srv.DB,
		Logger: srv.Logger,
		Banner: srv.Banner,
		DKIM:   dkimCheck,
		SPF:    spfCheck,
	}
	srv.RegisterService(srv.SMTP)

	srv.Cleanup = &cleanup.Cleanup{
		Config: *srv.Config.Cleanup,
		DB:     srv.DB,
		Logger: srv.Logger,
	}
	srv.RegisterService(srv.Cleanup)

	for _, service := range srv.RegisteredServices {
		if err := service.Start(); err != nil {
			log.Errorf("could not start service '%s': %v", service.Name(), err)
			return nil
		}

		srv.Logger.Infow("started service",
			logger.Fields{
				"service": service.Name(),
			},
		)
	}

	initConfig := srv.Config.Init
	if initConfig.Username != "" && initConfig.Password != "" {
		// TODO while we expect most of these errors should be ignored, it would
		// be nice to differentiate certain errors (e.g. DB errors) from
		// security ones.
		_ = letterbox.AddInitialAdminUser(
			srv.DB,
			srv.Hasher,
			&letterbox.User{
				Username: initConfig.Username,
				Password: initConfig.Password,
			})
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit
	srv.Logger.Infow("beginning shutdown")

	// Shutdown services in reverse order so their dependency chains stay valid
	for i := len(srv.RegisteredServices) - 1; i >= 0; i-- {
		service := srv.RegisteredServices[i]
		if err := service.Stop(); err != nil {
			log.Errorf("could not stop service %s: %s\n", service.Name(), err.Error())
		}

		srv.Logger.Infow("stopped service",
			logger.Fields{
				"service": service.Name(),
			},
		)
	}
	log.Info("server stopped")

	return nil
}

func (srv *Server) RegisterService(service Service) {
	srv.RegisteredServices = append(srv.RegisteredServices, service)
}

type Service interface {
	Name() string
	Start() error
	Stop() error
}

type LoggerService interface {
	Service
	logger.Logger
}
