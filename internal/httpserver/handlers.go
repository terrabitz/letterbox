package httpserver

import (
	"errors"
	"fmt"
	"net/http"
	"sync/atomic"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type statusResponse struct {
	Status  string `json:"status,omitempty"`
	Message string `json:"message,omitempty"`
}

func successResponse() statusResponse {
	return statusResponse{
		Status: "success",
	}
}

func errorResposne(err error) statusResponse {
	return statusResponse{
		Status:  "error",
		Message: err.Error(),
	}
}

func getStatusCodeForError(err error) int {
	var statusCodeMap map[error]int = map[error]int{
		letterbox.ErrNotFound:        http.StatusNotFound,
		letterbox.ErrAlreadyExists:   http.StatusBadRequest,
		letterbox.ErrValidationError: http.StatusBadRequest,
		ErrUnauthorized:              http.StatusUnauthorized,
		ErrDecode:                    http.StatusBadRequest,
	}

	for e, code := range statusCodeMap {
		if errors.Is(err, e) {
			return code
		}
	}

	return http.StatusInternalServerError
}

type AppHandler func(http.ResponseWriter, *http.Request) (interface{}, error)

type AppEndpoint struct {
	Handler AppHandler
	Server  *HTTPService
}

func (a AppEndpoint) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	responseObject, err := a.Handler(w, r)
	if err != nil {
		code := getStatusCodeForError(err)

		a.Server.Logger.InfoErr("status error", err,
			logger.Fields{
				"code": code,
			},
		)

		statusMessage := errorResposne(err)

		err := a.Server.respond(w, statusMessage, code)
		if err != nil {
			a.Server.Logger.Errorw("could not write out error message", err)
		}

		return
	}

	if responseObject != nil {
		_ = a.Server.respond(w, responseObject, http.StatusOK)
	}
}

func (s *HTTPService) handleUnauthorized() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		w.Header().Add("WWW-Authenticate", `Basic realm="letterbox"`)
		return nil, ErrUnauthorized
	})
}

// handleHello returns basic server information.
func (s *HTTPService) handleHello() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		return s.ServerInfo, nil
	})
}

func (s *HTTPService) handleHealthz() *AppEndpoint {
	type Ping struct {
		Status string `json:"status"`
	}

	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		if atomic.LoadInt32(&s.IsHealthy) == 0 {
			return nil, ErrServiceUnhealthy
		}
		return Ping{Status: "up"}, nil
	})
}

func (s *HTTPService) handleAddOneUser() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		user := &letterbox.User{}
		if err := decode(r, user); err != nil {
			return nil, err
		}

		if err := letterbox.AddUser(user, s.Hasher, s.DB); err != nil {
			return nil, err
		}

		return successResponse(), nil
	})
}

func (s *HTTPService) handleAddFirstUser() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		user := &letterbox.User{}
		if err := decode(r, user); err != nil {
			return nil, err
		}

		if err := letterbox.AddInitialAdminUser(s.DB, s.Hasher, user); err != nil {
			return nil, ErrUnauthorized
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleGetOneUser() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")
		u, err := s.DB.GetUser(paramUsername)
		if err != nil {
			return nil, fmt.Errorf("user '%s': %w", paramUsername, letterbox.ErrNotFound)
		}
		u.Scrub()
		return u, nil
	})
}

func (s *HTTPService) handleSetOneUser() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")
		body := &letterbox.User{}
		if err := decode(r, &body); err != nil {
			return nil, err
		}

		authenticatedUser, err := GetAuthenticatedUser(r)
		if err != nil {
			return nil, ErrUnauthorized
		}

		_, password, authOK := r.BasicAuth()
		if !authOK {
			return nil, ErrUnauthorized
		}

		// TODO move this to a centralized RBAC location
		if authenticatedUser.Username != paramUsername {
			return nil, fmt.Errorf("%w: a user may only edit their own account", ErrUnauthorized)
		}

		if err := letterbox.UpdateUser(paramUsername, password, body, s.Hasher, s.DB); err != nil {
			return nil, err
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleSetOneUserKey() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramUsername := getVar(r, "username")
		// TODO use a different model for inputs, rather than using our internal model
		body := &letterbox.UserKey{}
		if err := decode(r, &body); err != nil {
			return nil, err
		}

		authenticatedUser, err := GetAuthenticatedUser(r)
		if err != nil {
			return nil, ErrUnauthorized
		}

		_, password, authOK := r.BasicAuth()
		if !authOK {
			return nil, ErrUnauthorized
		}

		// TODO move this to a centralized RBAC location
		if authenticatedUser.Username != paramUsername {
			return nil, fmt.Errorf("%w: a user may only edit their own account", ErrUnauthorized)
		}

		if err := letterbox.UpdateUserKey(authenticatedUser, body, password, s.DB); err != nil {
			return nil, err
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleGetAllUsers() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		users, err := s.DB.GetAllUsers()
		if err != nil {
			return nil, fmt.Errorf("could get users: %w", err)
		}
		for i := 0; i < len(users); i++ {
			users[i].Scrub()
		}
		return users, nil
	})
}

func (s *HTTPService) handleAddOneDomain() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		d := &letterbox.Domain{}
		if err := decode(r, &d); err != nil {
			return nil, err
		}

		if err := s.DB.AddDomain(d); err != nil {
			return nil, fmt.Errorf("could not add domain '%s': %w", d.Domain, err)
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleGetOneDomain() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")
		domain, err := letterbox.GetDomain(paramDomain, s.DB)
		if err != nil {
			return nil, err
		}

		return domain, nil
	})
}

func (s *HTTPService) handleDeleteOneDomain() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")
		err := letterbox.DeleteDomain(paramDomain, s.DB)
		if err != nil {
			return nil, err
		}

		return successResponse(), nil
	})
}

func (s *HTTPService) handleGetAllDomains() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		domains, err := s.DB.GetAllDomains()
		if err != nil {
			return nil, fmt.Errorf("could not get domains: %w", err)
		}
		return domains, nil
	})
}

func (s *HTTPService) handleGetAllMailboxes() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")
		domain, err := letterbox.GetDomain(paramDomain, s.DB)
		if err != nil {
			return nil, err
		}

		m, err := s.DB.GetMailboxesByDomain(domain)
		if err != nil {
			return nil, fmt.Errorf("can't retrieve mailboxes for domain '%s': %w", paramDomain, err)
		}
		return m, nil
	})
}

func (s *HTTPService) handleGetOneMailbox() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := GetMailboxByURLPath(r, s.DB)
		if err != nil {
			return nil, err
		}

		return mailbox, nil
	})
}

func (s *HTTPService) handleSetOneMailbox() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		params := getVars(r)
		paramDomain := params.ByName("domain")
		paramName := params.ByName("name")

		body := &letterbox.Mailbox{}
		if err := decode(r, body); err != nil {
			return nil, err
		}
		if err := letterbox.UpdateMailbox(paramDomain, paramName, body, s.DB); err != nil {
			return nil, err
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleDeleteOneMailbox() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		params := getVars(r)
		paramDomain := params.ByName("domain")
		paramName := params.ByName("name")

		if err := letterbox.DeleteMailbox(paramDomain, paramName, s.DB); err != nil {
			return nil, err
		}
		return successResponse(), nil
	})
}

func (s *HTTPService) handleAddOneMailbox() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramDomain := getVar(r, "domain")
		user, err := GetAuthenticatedUser(r)
		if err != nil {
			return nil, err
		}
		mailbox := &letterbox.Mailbox{}
		if err := decode(r, &mailbox); err != nil {
			return nil, err
		}

		if err := letterbox.AddMailbox(paramDomain, user, mailbox, s.DB); err != nil {
			return nil, err
		}

		return successResponse(), nil
	})
}

func (s *HTTPService) handleDisableMailbox() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		paramID := getVar(r, "id")

		if err := letterbox.DisableMailboxByExternalID(paramID, s.DB, s.DB); err != nil {
			return nil, err
		}

		return successResponse(), nil
	})
}

func (s *HTTPService) handleGetMessages() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := GetMailboxByURLPath(r, s.DB)
		if err != nil {
			return nil, err
		}

		user, err := GetAuthenticatedUser(r)
		if err != nil {
			return nil, err
		}

		_, password, _ := r.BasicAuth()

		messages, err := letterbox.GetMessages(mailbox, user, password, s.DB)
		if err != nil {
			return nil, fmt.Errorf("error retrieving messages for mailbox '%s': %w", mailbox.Address, err)
		}

		return messages, nil
	})
}

func (s *HTTPService) handleGetMailboxStats() *AppEndpoint {
	return s.AddEndpoint(func(w http.ResponseWriter, r *http.Request) (interface{}, error) {
		mailbox, err := GetMailboxByURLPath(r, s.DB)
		if err != nil {
			return nil, err
		}

		stats, err := s.DB.GetStats(mailbox)
		if err != nil {
			return nil, fmt.Errorf("error retrieving stats for mailbox '%s': %w", mailbox.Address, err)
		}

		return stats, nil
	})
}

func GetMailboxByURLPath(r *http.Request, db letterbox.Datastore) (*letterbox.Mailbox, error) {
	params := getVars(r)
	paramDomain := params.ByName("domain")
	paramName := params.ByName("name")

	mailbox, err := letterbox.GetMailbox(paramDomain, paramName, db)
	if err != nil {
		return nil, err
	}

	return mailbox, nil
}
