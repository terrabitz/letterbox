package httpserver_test

import (
	"reflect"
	"testing"

	. "gitlab.com/hackandsla.sh/letterbox/internal/httpserver"
)

func Test_reverseStringSlice(t *testing.T) {
	tests := []struct {
		name  string
		input []string
		want  []string
	}{
		{"reverse a string array", []string{"foo", "example", "com"}, []string{"com", "example", "foo"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReverseStringSlice(tt.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("reverseStringSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
