package httpserver

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/jaredfolkins/badactor"
	"github.com/julienschmidt/httprouter"
	metrics "github.com/slok/go-http-metrics/metrics/prometheus"
	"github.com/slok/go-http-metrics/middleware"
	"github.com/unrolled/secure"

	"gitlab.com/hackandsla.sh/letterbox/internal/badactorservice"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

const tcpMax int = 65535

type Config struct {
	MaxShutdownTime time.Duration `json:"max_shutdown_time,omitempty"`
	Port            int           `json:"port,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Port, validation.Required, validation.Min(1), validation.Max(tcpMax)),
	)
}

type HTTPService struct {
	Config Config

	httpServer          *http.Server
	PerRouteMiddlewares []PerRouteMiddleware
	Router              *httprouter.Router
	IsHealthy           int32
	MaxShutdownTime     time.Duration

	// TODO turn this into a dependency
	badActors *badactor.Studio

	// Service Dependencies
	DB         letterbox.Datastore
	Logger     logger.Logger
	ServerInfo *letterbox.Info
	Hasher     letterbox.ArgonHasher
}

func (s *HTTPService) Name() string {
	return "HTTP"
}

func (s *HTTPService) Start() error {
	s.MaxShutdownTime = s.Config.MaxShutdownTime

	// TODO find another place to initialize badactor
	b := badactorservice.BadActorService{
		Config: badactorservice.Config{
			MaxAuthAttempts: 10,
			JailTime:        10 * time.Minute,
		},
		Logger: s.Logger,
	}

	err := b.Start()
	if err != nil {
		return err
	}

	s.badActors = b.Studio

	s.PerRouteMiddlewares = initPerRouteMiddleware()
	s.createRoutes()
	r := wrapMiddleware(
		s.Router,
		initGlobalMiddleware(s)...,
	)
	port := s.Config.Port
	portString := strconv.Itoa(port)
	listenAddr := ":" + portString

	s.httpServer = &http.Server{
		Addr:         listenAddr,
		Handler:      r,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	ip := getOutboundIP()
	ipPort := ip.String() + ":" + portString

	atomic.StoreInt32(&s.IsHealthy, 1)

	log := s.Logger.With(logger.Fields{
		"address": listenAddr,
		"ipport":  ipPort,
	})
	log.Infow("starting server")
	// TODO use a channel to propagate the error back up to the initializer
	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Errorw("could not listen on address", err)
		}
	}()

	log.Infow("server is ready to handle requests")

	return nil
}

func (s *HTTPService) Stop() error {
	atomic.StoreInt32(&s.IsHealthy, 0)

	ctx, cancel := context.WithTimeout(context.Background(), s.MaxShutdownTime)
	defer cancel()

	s.httpServer.SetKeepAlivesEnabled(false)

	if err := s.httpServer.Shutdown(ctx); err != nil {
		s.Logger.InfoErr("could not gracefully shutdown the server", err)
		return err
	}

	return nil
}

func (s *HTTPService) AddEndpoint(handler AppHandler) *AppEndpoint {
	return &AppEndpoint{
		Server:  s,
		Handler: handler,
	}
}

func initPerRouteMiddleware() []PerRouteMiddleware {
	metricsMiddleware := &EndpointMetricsMiddleware{
		MetricsCollector: middleware.New(middleware.Config{
			Recorder: metrics.NewRecorder(metrics.Config{}),
		})}

	return []PerRouteMiddleware{
		metricsMiddleware,
	}
}

func initGlobalMiddleware(s *HTTPService) []Middleware {
	featurePolicies := []string{
		"geolocation none",
		"midi none",
		"notifications none",
		"push none",
		"sync-xhr none",
		"microphone none",
		"camera none",
		"magnetometer none",
		"gyroscope none",
		"speaker self",
		"vibrate none",
		"payment none",
	}
	secureMiddleware := secure.New(secure.Options{
		FrameDeny:          true,
		BrowserXssFilter:   true,
		ContentTypeNosniff: true,
		ReferrerPolicy:     "same-origin",
		FeaturePolicy:      strings.Join(featurePolicies, ";"),
	})

	return []Middleware{
		secureMiddleware.Handler,
		BadActorMiddleware(s),
	}
}
