package httpserver

import (
	"fmt"
	"log"
	"net"
	"net/http"

	json "github.com/json-iterator/go"
	"github.com/julienschmidt/httprouter"
)

func getOutboundIP() net.IP {
	conn, err := net.Dial("udp", "1.1.1.1:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func (s *HTTPService) respond(w http.ResponseWriter, data interface{}, status int) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if data != nil {
		b, err := json.Marshal(data)
		if err != nil {
			s.Logger.Errorw("couldn't marshal JSON", err)

			return err
		}

		fmt.Fprint(w, string(b))
	}

	return nil
}

func decode(r *http.Request, v interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		return ErrDecode
	}

	return nil
}

func getVars(r *http.Request) httprouter.Params {
	return httprouter.ParamsFromContext(r.Context())
}

func getVar(r *http.Request, param string) string {
	params := httprouter.ParamsFromContext(r.Context())
	return params.ByName(param)
}

func ReverseStringSlice(s []string) []string {
	lenArr := len(s)
	ret := make([]string, lenArr)

	for i := lenArr - 1; i >= 0; i-- {
		ret[lenArr-i-1] = s[i]
	}

	return ret
}
