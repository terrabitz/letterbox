package httpserver

import (
	"context"
	"net"
	"net/http"

	"github.com/slok/go-http-metrics/middleware"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type key int

const (
	AuthenticatedUserKey key = iota + 1
)

type Middleware func(http.Handler) http.Handler

func (s *SubRouter) AddMiddleware(m ...Middleware) {
	s.Middlewares = append(s.Middlewares, m...)
}

func wrapMiddleware(handler http.Handler, middlewares ...Middleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m(newHandler)
	}

	return newHandler
}

// PerRouteMiddleware adds middleware that requires information about the specific
// paths used (e.g. HTTP metric collection, which needs to know what path a given
// function is registered to).
type PerRouteMiddleware interface {
	AddMiddleware(path string, handler http.Handler) http.Handler
}

func wrapPerRouteMiddleware(path string, handler http.Handler, middlewares ...PerRouteMiddleware) http.Handler {
	newHandler := handler
	// Apply middlewares in reverse so the most specific ones lie closest to the handler
	for i := len(middlewares) - 1; i >= 0; i-- {
		m := middlewares[i]
		newHandler = m.AddMiddleware(path, newHandler)
	}

	return newHandler
}

type EndpointMetricsMiddleware struct {
	MetricsCollector middleware.Middleware
}

func (m *EndpointMetricsMiddleware) AddMiddleware(path string, handler http.Handler) http.Handler {
	return m.MetricsCollector.Handler(path, handler)
}

func AuthMiddleware(s *HTTPService) Middleware {
	return Middleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			presentLogin := s.handleUnauthorized()
			username, password, authOK := r.BasicAuth()
			if !authOK {
				presentLogin.ServeHTTP(w, r)
				return
			}

			remoteIP, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				s.Logger.InfoErr("could not get remote IP", err)
				presentLogin.ServeHTTP(w, r)
				return
			}
			log := s.Logger.With(
				logger.Fields{
					"ip":   remoteIP,
					"user": username,
				},
			)

			user, err := s.DB.GetUser(username)
			if err != nil {
				if infractionErr := s.badActors.Infraction(remoteIP, "Login"); infractionErr != nil {
					log.Errorw("error while adding badactor infraction", infractionErr)
				}
				log.InfoErr("invalid username attempted", err)
				presentLogin.ServeHTTP(w, r)
				return
			}

			valid, err := s.Hasher.Validate(password, user.Password)
			if err != nil {
				log.InfoErr("error while looking up password", err)
				presentLogin.ServeHTTP(w, r)
				return
			}

			if !valid {
				if infractionErr := s.badActors.Infraction(remoteIP, "Login"); infractionErr != nil {
					log.Errorw("error while adding badactor infraction", err)
				}
				log.Infow("invalid password attempted")
				presentLogin.ServeHTTP(w, r)
				return
			}
			log.Debugw("login succeeded")
			r = AddValueToContext(r, AuthenticatedUserKey, user)
			next.ServeHTTP(w, r)
		})
	})
}

func GetAuthenticatedUser(r *http.Request) (*letterbox.User, error) {
	user, ok := r.Context().Value(AuthenticatedUserKey).(*letterbox.User)
	if !ok {
		return nil, ErrUnauthorized
	}

	return user, nil
}

func BadActorMiddleware(s *HTTPService) Middleware {
	return Middleware(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			remoteIP, _, err := net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				s.Logger.InfoErr("could not get remote IP", err)
				next.ServeHTTP(w, r)
			}
			if s.badActors.IsJailed(remoteIP) {
				http.Error(w, "IP is currently banned due to failed login attempts", http.StatusForbidden)
				return
			}
			next.ServeHTTP(w, r)
		})
	})
}

func AddValueToContext(r *http.Request, key key, value interface{}) *http.Request {
	ctx := context.WithValue(r.Context(), key, value)
	return r.WithContext(ctx)
}
