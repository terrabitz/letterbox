package validate

import (
	"errors"
	"fmt"
	"os"

	"github.com/docker/go-units"
)

var (
	ErrStringConvert = errors.New("could not convert value to string")
	ErrInvalidSize   = errors.New("invalid size")
	ErrFileNotFound  = errors.New("file not found")
)

func MaxSize(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return fmt.Errorf("'%v': %w", value, ErrStringConvert)
	}

	_, err := units.FromHumanSize(s)
	if err != nil {
		return fmt.Errorf("'%s': %w", s, ErrInvalidSize)
	}

	return nil
}

func FileExists(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return fmt.Errorf("'%v': %w", value, ErrStringConvert)
	}

	if s == "" {
		return nil
	}

	if _, err := os.Stat(s); os.IsNotExist(err) {
		return fmt.Errorf("'%s': %w", s, ErrFileNotFound)
	}

	return nil
}
