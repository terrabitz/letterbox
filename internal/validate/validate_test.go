package validate_test

import (
	"testing"

	. "gitlab.com/hackandsla.sh/letterbox/internal/validate"
)

func Test_MaxSize(t *testing.T) {
	tests := []struct {
		name    string
		value   interface{}
		wantErr bool
	}{
		{"interprets size", "25MB", false},
		{"interprets size as bytes", "25", false},
		{"errors if input isn't string", 25, true},
		{"doesn't parse incorrect strings", "asdf", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := MaxSize(tt.value); (err != nil) != tt.wantErr {
				t.Errorf("MaxSize() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
