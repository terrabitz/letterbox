package banner

import (
	"strings"
	"text/template"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type Config struct {
	// The public URI to use with redirection links
	URL string

	// The template to use for banners
	Template string
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Template, validation.Required),
	)
}

type Builder struct {
	url            string
	bannerTemplate *template.Template
}

const Default = `
<div>
	<p>This mail was proxied by Letterbox</p>
	<br />
	{{ if and (ne .URL "") (ne .ID "") -}}
	<a href="{{ .URL }}/disable/{{ .ID }}">Disable Mailbox</a>
	{{- end }}
</div>
`

func NewBuilder(config *Config) (*Builder, error) {
	t, err := template.New("banner").Parse(config.Template)
	if err != nil {
		return nil, err
	}

	return &Builder{
		url:            config.URL,
		bannerTemplate: t,
	}, nil
}

type templateVars struct {
	URL string
	ID  string
}

func (b *Builder) Make(mailboxID string) (string, error) {
	tv := templateVars{
		URL: b.url,
		ID:  mailboxID,
	}

	var builder strings.Builder

	if err := b.bannerTemplate.Execute(&builder, tv); err != nil {
		return "", err
	}

	return builder.String(), nil
}
