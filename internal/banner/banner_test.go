package banner_test

import (
	"testing"

	. "gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestBuilder_Make(t *testing.T) {
	type args struct {
		mailboxID string
	}

	tests := []struct {
		name    string
		args    args
		builder *Builder
		want    string
		wantErr bool
	}{
		{
			name: "Builds a banner given an ID",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
				test.WithTemplate(Default),
			),
			want: `
<div>
	<p>This mail was proxied by Letterbox</p>
	<br />
	<a href="foo.example.com/disable/abcdef">Disable Mailbox</a>
</div>
`,
		},
		{
			name: "Excludes mailbox disable link if hostname not specified",
			args: args{
				mailboxID: "abcdef",
			},
			builder: test.BannerBuilder(
				test.WithURL(""),
				test.WithTemplate(Default),
			),
			want: `
<div>
	<p>This mail was proxied by Letterbox</p>
	<br />
	
</div>
`,
		},
		{
			name: "Excludes mailbox disable link if ID not specified",
			args: args{
				mailboxID: "",
			},
			builder: test.BannerBuilder(
				test.WithURL("foo.example.com"),
				test.WithTemplate(Default),
			),
			want: `
<div>
	<p>This mail was proxied by Letterbox</p>
	<br />
	
</div>
`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.builder.Make(tt.args.mailboxID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Builder.Make() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Builder.Make() = %v, want %v", got, tt.want)
			}
		})
	}
}
