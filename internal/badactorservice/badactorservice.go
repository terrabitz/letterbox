package badactorservice

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/jaredfolkins/badactor"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

type Config struct {
	MaxAuthAttempts int           `json:"max_auth_attempts,omitempty"`
	JailTime        time.Duration `json:"jail_time,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.MaxAuthAttempts, validation.Required, validation.Min(1)),
		validation.Field(&c.JailTime, validation.Required),
	)
}

type BadActorService struct {
	Config Config

	Studio *badactor.Studio

	Logger logger.Logger
}

func (b *BadActorService) Start() error {
	var maxStudios int32 = 1024

	var maxDirectors int32 = 1024

	pollDuration := time.Second * 60
	studio := badactor.NewStudio(maxStudios)
	rule := &badactor.Rule{
		Name:        "Login",
		Message:     "You have failed to login too many times",
		StrikeLimit: b.Config.MaxAuthAttempts,
		ExpireBase:  b.Config.JailTime,
		Sentence:    b.Config.JailTime,
		Action: &BadActorAction{
			Logger: b.Logger,
		},
	}
	studio.AddRule(rule)

	err := studio.CreateDirectors(maxDirectors)
	if err != nil {
		return err
	}

	studio.StartReaper(pollDuration)
	b.Studio = studio

	return nil
}

type BadActorAction struct {
	Logger logger.Logger
}

func (b *BadActorAction) WhenJailed(a *badactor.Actor, r *badactor.Rule) error {
	b.Logger.Infow("bad actor banned",
		logger.Fields{
			"rule":    r.Name,
			"message": r.Message,
			// "ip", a.Name // TODO This depends on https://github.com/jaredfolkins/badactor/pull/7
		},
	)

	return nil
}

func (b *BadActorAction) WhenTimeServed(a *badactor.Actor, r *badactor.Rule) error {
	b.Logger.Infow("bad actor unbanned",
		logger.Fields{
			"rule": r.Name,
			// "ip", a.Name // TODO This depends on https://github.com/jaredfolkins/badactor/pull/7
		},
	)

	return nil
}
