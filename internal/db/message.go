package db

import "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"

func (db *SQLDatastore) AddMessage(mb *letterbox.Mailbox, m *letterbox.EncryptedMessage) error {
	m.MailboxID = mb.ID
	if err := db.DB.Create(m).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) GetMessages(mb *letterbox.Mailbox) ([]letterbox.EncryptedMessage, error) {
	messages := []letterbox.EncryptedMessage{}
	if err := db.DB.Model(&mb).Related(&messages).Error; err != nil {
		return nil, err
	}

	return messages, nil
}

func (db *SQLDatastore) UpdateMessage(m *letterbox.EncryptedMessage) error {
	return db.DB.Save(m).Error
}

func (db *SQLDatastore) DeleteMessage(m *letterbox.EncryptedMessage) error {
	if m.ID == 0 {
		return letterbox.ErrMissingID
	}

	return db.DB.Unscoped().Delete(m).Error
}

func (db *SQLDatastore) GetMessagesForUser(u *letterbox.User) ([]letterbox.EncryptedMessage, error) {
	var mailboxes []letterbox.Mailbox

	if err := db.DB.Model(&u).Related(&mailboxes).Error; err != nil {
		return nil, err
	}

	var encryptedMessages []letterbox.EncryptedMessage

	for i := 0; i < len(mailboxes); i++ {
		mailbox := mailboxes[i]

		m, err := db.GetMessages(&mailbox)
		if err != nil {
			return nil, err
		}

		encryptedMessages = append(encryptedMessages, m...)
	}

	return encryptedMessages, nil
}
