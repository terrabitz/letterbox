package db

import (
	"fmt"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func (db *SQLDatastore) AddUser(u *letterbox.User) error {
	if err := db.DB.Create(u).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) GetUser(name string) (*letterbox.User, error) {
	u := &letterbox.User{}
	if err := db.DB.Where(&letterbox.User{Username: name}).First(&u).Error; err != nil {
		return nil, err
	}

	return u, nil
}

func (db *SQLDatastore) UpdateUser(u *letterbox.User) error {
	if err := db.DB.Save(u).Error; err != nil {
		return err
	}

	return nil
}

func (db *SQLDatastore) GetAllUsers() ([]letterbox.User, error) {
	users := []letterbox.User{}
	if err := db.DB.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (db *SQLDatastore) GetMailboxUser(mailbox *letterbox.Mailbox) (*letterbox.User, error) {
	if mailbox.UserID == 0 {
		return nil, fmt.Errorf("%w: mailbox '%s' has no attached user", letterbox.ErrInvalidValue, mailbox.Address)
	}

	q := &letterbox.User{}
	q.ID = mailbox.UserID
	user := &letterbox.User{}

	if err := db.DB.Where(q).First(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}
