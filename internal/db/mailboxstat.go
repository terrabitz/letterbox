package db

import "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"

func (db *SQLDatastore) GetStats(mailbox *letterbox.Mailbox) ([]letterbox.DomainStat, error) {
	stats := []letterbox.DomainStat{}
	if err := db.DB.Model(&mailbox).Order("count desc").Related(&stats).Error; err != nil {
		return nil, err
	}

	return stats, nil
}

func (db *SQLDatastore) GetStat(mailbox *letterbox.Mailbox, domain string) (*letterbox.DomainStat, error) {
	stat := &letterbox.DomainStat{}
	q := &letterbox.DomainStat{
		MailboxID: mailbox.ID,
		Domain:    domain,
	}

	if err := db.DB.Where(q).First(stat).Error; err != nil {
		return nil, err
	}

	return stat, nil
}

func (db *SQLDatastore) UpdateStat(stat *letterbox.DomainStat) error {
	return db.DB.Save(stat).Error
}

func (db *SQLDatastore) CreateStat(mailbox *letterbox.Mailbox, stat *letterbox.DomainStat) error {
	stat.MailboxID = mailbox.ID
	return db.DB.Create(stat).Error
}
