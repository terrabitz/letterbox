package smtpserver

import (
	"crypto/tls"
	"fmt"
	"time"

	"github.com/docker/go-units"
	smtp "github.com/emersion/go-smtp"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/smtpclient"
	"gitlab.com/hackandsla.sh/letterbox/internal/validate"
)

const tcpMax int = 65535

type Config struct {
	Hostname string             `json:"hostname,omitempty"`
	Port     int                `json:"port,omitempty"`
	MaxSize  string             `json:"max_size,omitempty"`
	Outbound *smtpclient.Config `json:"outbound,omitempty"`
	TLS      *TLSConfig         `json:"tls,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Hostname, validation.Required, is.Host),
		validation.Field(&c.Port, validation.Required, validation.Min(1), validation.Max(tcpMax)),
		validation.Field(&c.MaxSize, validation.Required, validation.By(validate.MaxSize)),
		validation.Field(&c.Outbound),
		validation.Field(&c.TLS),
	)
}

type TLSConfig struct {
	// The certificate string
	Cert string `json:"cert,omitempty"`

	// The (unencrypted) private key string
	Key string `json:"key,omitempty"`

	// The path to the file containing the certificate
	CertFile string `json:"cert_file,omitempty"`

	// The path to the file containing the (unencrypted) private key
	KeyFile string `json:"key_file,omitempty"`

	// The port to use for the TLS-only SMTPS server
	// This server is wrapped with TLS, and doesn't use the STARTTLS function
	// See RFC8314
	Port int `json:"port,omitempty"`
}

func (c *TLSConfig) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.CertFile, validation.By(validate.FileExists)),
		validation.Field(&c.KeyFile, validation.By(validate.FileExists)),
	)
}

type SMTPService struct {
	Config           Config
	Server           *smtp.Server
	TLSServer        *smtp.Server
	GlobalSMTPClient smtpClient
	MaxSize          int64

	// Service Dependencies
	DB     letterbox.Datastore
	Logger logger.Logger
	Banner *banner.Builder
	DKIM   dkimChecker
	SPF    spfChecker
}

func (ss *SMTPService) Name() string {
	return "SMTP"
}

func (ss *SMTPService) Start() error {
	if ss.DB == nil {
		return fmt.Errorf("%w: DB", ErrMissingDependency)
	}

	if ss.Logger == nil {
		return fmt.Errorf("%w: logger", ErrMissingDependency)
	}

	if ss.Banner == nil {
		return fmt.Errorf("%w: banner", ErrMissingDependency)
	}

	maxSize, err := units.FromHumanSize(ss.Config.MaxSize)
	if err != nil {
		return err
	}

	ss.MaxSize = maxSize

	if ss.Config.Outbound.Host != "" {
		ss.GlobalSMTPClient, err = smtpclient.New(ss.Config.Outbound, ss.Logger)
		if err != nil {
			return fmt.Errorf("error while initializing SMTP client: %w", err)
		}
	} else {
		ss.Logger.Infow("no outbound SMTP server specified; skipping client configuration")
	}

	tlsConfig, err := GetTLSConfig(ss.Config.TLS)
	if err != nil {
		return err
	} else if tlsConfig == nil {
		ss.Logger.Infow("no TLS configuration specified; skipping TLS server creation")
	}

	ss.Server = NewBackend(ss, tlsConfig, ss.Config.Port, int(maxSize), ss.Config.Hostname)

	if tlsConfig != nil {
		ss.TLSServer = NewBackend(ss, tlsConfig, ss.Config.TLS.Port, int(maxSize), ss.Config.Hostname)
	}

	// TODO check for errors on startup
	if ss.Server != nil {
		ss.Logger.Infow("starting SMTP server",
			logger.Fields{
				"address": ss.Server.Addr,
			},
		)

		go func() {
			if err := ss.Server.ListenAndServe(); err != nil {
				ss.Logger.InfoErr("error while starting up HTTP server", err)
			}
		}()
	}

	if ss.TLSServer != nil {
		ss.Logger.Infow("starting TLS-only SMTP server",
			logger.Fields{
				"address": ss.TLSServer.Addr,
			},
		)

		go func() {
			if err := ss.TLSServer.ListenAndServeTLS(); err != nil {
				ss.Logger.InfoErr("error while starting up HTTPS servers", err)
			}
		}()
	}

	ss.Logger.Infow("SMTP service started")

	return nil
}

func NewBackend(ss smtp.Backend, tc *tls.Config, port, maxSize int, hostname string) *smtp.Server {
	smtpServer := smtp.NewServer(ss)

	smtpServer.TLSConfig = tc
	smtpServer.Addr = fmt.Sprintf(":%d", port)
	smtpServer.Domain = hostname
	smtpServer.MaxMessageBytes = maxSize

	// TODO expose these as higher-level variables
	smtpServer.ReadTimeout = 100 * time.Second
	smtpServer.WriteTimeout = 100 * time.Second
	smtpServer.MaxRecipients = 50
	smtpServer.AuthDisabled = true

	return smtpServer
}

func (ss *SMTPService) Stop() error {
	if ss.Server != nil {
		ss.Server.Close()
	}

	if ss.TLSServer != nil {
		ss.TLSServer.Close()
	}

	return nil
}

func GetTLSConfig(c *TLSConfig) (*tls.Config, error) {
	var certificate tls.Certificate

	var err error

	if c.Cert != "" && c.Key != "" {
		certificate, err = tls.X509KeyPair([]byte(c.Cert), []byte(c.Key))
	} else if c.CertFile != "" && c.KeyFile != "" {
		certificate, err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
	} else {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &tls.Config{
		Certificates: []tls.Certificate{
			certificate,
		},
		MinVersion: tls.VersionTLS12,
	}, nil
}
