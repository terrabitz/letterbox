package spf

import (
	"fmt"
	"net"

	"blitiri.com.ar/go/spf"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

//go:generate go-enum -f=$GOFILE --marshal
// Status is a description of the DKIM status
/* ENUM(
	none
	fail
	pass
	unknown
)
*/
type Status int

type Client struct {
	DomainAlignment DomainAlignmentCheck
}

type DomainAlignmentCheck interface {
	ValidateDomainAlignment(a, b string) error
}

func (c *Client) GetStatus(remoteIP net.IP, helo, fromHeader string) (Status, error) {
	result, err := spf.CheckHostWithSender(remoteIP, helo, fromHeader)

	var status Status

	switch result {
	case spf.None, spf.Neutral:
		status = StatusNone
	case spf.Pass:
		status = StatusPass
	case spf.Fail:
		status = StatusFail
	case spf.SoftFail, spf.TempError, spf.PermError:
		status = StatusUnknown
	}

	if status == StatusPass {
		// CheckHostWithSender uses the error to convey contextual information even
		// in cases where the check passes. We don't care about this contextual
		// info, so we'll just nil this error if the check succeeds
		err = nil
	}

	return status, err
}

func (c *Client) ValidateAlignment(from, fromHeader string) (bool, error) {
	addr1, err := letterbox.SplitAddr(from)
	if err != nil {
		return false, fmt.Errorf("%w: could not parse MailFrom: %v", letterbox.ErrParsingError, err)
	}

	addr2, err := letterbox.SplitAddr(fromHeader)
	if err != nil {
		return false, fmt.Errorf("%w: could not parse from header: %v", letterbox.ErrParsingError, err)
	}

	err = c.DomainAlignment.ValidateDomainAlignment(addr1.Domain, addr2.Domain)

	return err == nil, err
}
