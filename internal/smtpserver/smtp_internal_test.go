package smtpserver

import "testing"

func Test_validateDomainAllowlist(t *testing.T) {
	type args struct {
		allowlist []string
		domain    string
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Allow a domain when allowlist is nil",
			args: args{
				allowlist: nil,
				domain:    "foo.example.com",
			},
			wantErr: false,
		},
		{
			name: "Allow a domain when allowlist is empty",
			args: args{
				allowlist: []string{},
				domain:    "foo.example.com",
			},
			wantErr: false,
		},
		{
			name: "Allow a domain on the allowlist",
			args: args{
				allowlist: []string{
					"*.example.com",
				},
				domain: "foo.example.com",
			},
			wantErr: false,
		},
		{
			name: "Dissallow a domain not on the allowlist",
			args: args{
				allowlist: []string{
					"bar.example.com",
					"baz.example.com",
				},
				domain: "foo.example.com",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateDomainAllowlist(tt.args.allowlist, tt.args.domain); (err != nil) != tt.wantErr {
				t.Errorf("validateDomainAllowlist() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
