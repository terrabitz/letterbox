package dkim

import (
	"fmt"

	"github.com/toorop/go-dkim"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

//go:generate go-enum -f=$GOFILE --marshal
// Status is a description of the DKIM status
/* ENUM(
	none
	fail
	pass
	unknown
)
*/
type Status int

type Client struct {
	DomainAlignment DomainAlignmentCheck
}

type DomainAlignmentCheck interface {
	ValidateDomainAlignment(a, b string) error
}

func (c *Client) GetStatus(raw []byte) (Status, error) {
	statusCode, err := dkim.Verify(&raw)
	if err != nil {
		return StatusUnknown, err
	}

	var status Status

	switch statusCode {
	case dkim.NOTSIGNED:
		status = StatusNone
	case dkim.PERMFAIL:
		status = StatusFail
	case dkim.SUCCESS:
		status = StatusPass
	case dkim.TEMPFAIL, dkim.TESTINGPERMFAIL, dkim.TESTINGSUCCESS, dkim.TESTINGTEMPFAIL:
		status = StatusUnknown
	}

	return status, nil
}

func (c *Client) ValidateAlignment(raw []byte, from string) (bool, error) {
	dkimHeader, err := dkim.GetHeader(&raw)
	if err != nil {
		return false, fmt.Errorf("%w: couldn't extract DKIM header: %v", letterbox.ErrParsingError, err)
	}

	addr, err := letterbox.SplitAddr(from)
	if err != nil {
		return false, fmt.Errorf("%w: couldn't parse from header: %v", letterbox.ErrParsingError, err)
	}

	err = c.DomainAlignment.ValidateDomainAlignment(addr.Domain, dkimHeader.Domain)

	return err == nil, err
}
