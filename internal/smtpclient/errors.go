package smtpclient

import "errors"

var (
	ErrFailedSend  = errors.New("failed to send email")
	ErrFailedInit  = errors.New("could not connect to outbound SMTP server")
	ErrFailedClose = errors.New("could not close connection to outbound SMTP server")
)
