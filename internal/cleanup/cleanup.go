package cleanup

import (
	"errors"
	"fmt"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

var ErrMissingDependency = errors.New("missing dependency")

type Config struct {
	Interval time.Duration `json:"interval,omitempty"`
}

func (c *Config) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.Interval, validation.Required),
	)
}

type Cleanup struct {
	quit   chan interface{}
	Config Config

	DB     letterbox.Datastore
	Logger logger.Logger
}

func (cs *Cleanup) Name() string {
	return "Cleanup"
}

func (cs *Cleanup) Start() error {
	if cs.DB == nil {
		return fmt.Errorf("%w: DB", ErrMissingDependency)
	}

	if cs.Logger == nil {
		return fmt.Errorf("%w: logger", ErrMissingDependency)
	}

	ticker := time.NewTicker(cs.Config.Interval)
	cs.quit = make(chan interface{})

	go func() {
		for {
			select {
			case <-ticker.C:
				if err := letterbox.DeleteExpiredMailboxes(cs.DB, cs.Logger); err != nil {
					cs.Logger.InfoErr("error while removing expired mailboxes", err)
				}
			case <-cs.quit:
				ticker.Stop()
				return
			}
		}
	}()

	return nil
}

func (cs *Cleanup) Stop() error {
	close(cs.quit)
	return nil
}
