package letterbox

import "errors"

var (
	ErrNotFound            = errors.New("not found")
	ErrBackendError        = errors.New("couldn't complete database operation")
	ErrValidationError     = errors.New("validation failed")
	ErrAlreadyExists       = errors.New("already exists")
	ErrInitError           = errors.New("could not initialize service")
	ErrMissingDependency   = errors.New("missing dependency")
	ErrInvalidDestination  = errors.New("invalid destination")
	ErrParsingError        = errors.New("parsing error")
	ErrDomainMisalignment  = errors.New("domains not aligned")
	ErrHashFail            = errors.New("error while hashing password")
	ErrSMTPClientInitFail  = errors.New("could not connect to outbound SMTP server")
	ErrSMTPClientCloseFail = errors.New("could not close connection to outbound SMTP server")
	ErrSMTPClientSendFail  = errors.New("could not send SMTP message")
	ErrMaxSizeExceeded     = errors.New("maximum message size exceeded")
	ErrMissingID           = errors.New("missing ID")
	ErrPasswordInvalid     = errors.New("password invalid")
	ErrInvalidValue        = errors.New("invalid value")
)
