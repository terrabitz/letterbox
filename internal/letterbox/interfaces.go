package letterbox

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

// Datastore embeds the datastores for each domain object.
//counterfeiter:generate . Datastore
type Datastore interface {
	DomainDatastore
	UserDatastore
	UserKeyDatastore
	MailboxDatastore
	MailboxStatsDatastore
	MessageDatastore
}
