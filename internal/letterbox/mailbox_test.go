package letterbox_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func Test_updateMailbox(t *testing.T) {
	type args struct {
		domain   string
		name     string
		newValue *Mailbox
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
		getDB   func() *letterboxfakes.FakeDatastore
	}{
		{
			name: "Should update the enabled state on a mailbox",
			args: args{
				domain: "foo.com",
				name:   "foo",
				newValue: &Mailbox{
					Enabled:    false,
					IsCatchAll: false,
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{
					Enabled: true,
					Name:    "foo",
				}, nil)
				return db
			},
		},
		{
			name: "Should not enable catch-all if there already exists a catch-all mailbox",
			args: args{
				domain: "foo.com",
				name:   "foo",
				newValue: &Mailbox{
					IsCatchAll: true,
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{
					Enabled: true,
					Name:    "foo",
				}, nil)
				db.GetCatchAllReturns(&Mailbox{
					Name:       "bar",
					IsCatchAll: true,
				}, nil)
				return db
			},
			wantErr: true,
		},
		{
			name: "Should add a message with an expiry time",
			args: args{
				domain: "foo.com",
				name:   "foo",
				newValue: &Mailbox{
					Expires: func() *time.Time {
						t := time.Now().Add(time.Hour)
						return &t
					}(),
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{
					Enabled: true,
					Name:    "foo",
				}, nil)
				return db
			},
			wantErr: false,
		},
		{
			name: "Should not add a message if the expiry time is in the past",
			args: args{
				domain: "foo.com",
				name:   "foo",
				newValue: &Mailbox{
					Expires: func() *time.Time {
						t := time.Now().Add(-1 * time.Hour)
						return &t
					}(),
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{
					Enabled: true,
					Name:    "foo",
				}, nil)
				return db
			},
			wantErr: true,
		},
		{
			name: "Error during update",
			args: args{
				domain: "foo.com",
				name:   "foo",
				newValue: &Mailbox{
					Enabled:    false,
					IsCatchAll: false,
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{
					Enabled: true,
					Name:    "foo",
				}, nil)
				db.UpdateMailboxReturns(errors.New("couldn't update mailbox"))
				return db
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := tt.getDB()
			if err := UpdateMailbox(tt.args.domain, tt.args.name, tt.args.newValue, db); (err != nil) != tt.wantErr {
				t.Errorf("updateMailbox() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_addMailbox(t *testing.T) {
	Assert := assert.New(t)

	type args struct {
		domain  string
		mailbox *Mailbox
	}

	tests := []struct {
		name       string
		args       args
		getDB      func() *letterboxfakes.FakeDatastore
		validateDB func(db *letterboxfakes.FakeDatastore)
		wantErr    bool
	}{
		{
			name: "Should add a new mailbox to an existing domain",
			args: args{
				domain: "foo.com",
				mailbox: &Mailbox{
					Name: "rick",
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(nil, errors.New("mailbox 'rick' doesn't exist"))
				return db
			},
			validateDB: func(db *letterboxfakes.FakeDatastore) {
				_, _, addedMessage := db.AddMailboxArgsForCall(0)
				Assert.Equal("rick@foo.com", addedMessage.Address)
				Assert.Regexp(".{20}@foo\\.com", addedMessage.ProxyAddress)
			},
		},
		{
			name: "Should return an error if mailbox is invalid",
			args: args{
				domain: "foo.com",
				mailbox: &Mailbox{
					Name: "",
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				return db
			},
			wantErr: true,
		},
		{
			name: "Should error if the mailbox already exists",
			args: args{
				domain: "foo.com",
				mailbox: &Mailbox{
					Name: "rick",
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(&Mailbox{Name: "rick"}, nil)
				return db
			},
			wantErr: true,
		},
		{
			name: "Should error if the call to save the message fails",
			args: args{
				domain: "foo.com",
				mailbox: &Mailbox{
					Name: "rick",
				},
			},
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetDomainReturns(&Domain{Domain: "foo.com"}, nil)
				db.GetMailboxByDomainReturns(nil, errors.New("mailbox 'rick' doesn't exist"))
				db.AddMailboxReturns(errors.New("couldn't add message"))
				return db
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := tt.getDB()
			user := &User{}
			user.ID = 123
			if err := AddMailbox(tt.args.domain, user, tt.args.mailbox, db); (err != nil) != tt.wantErr {
				t.Errorf("addMailbox() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.validateDB != nil {
				tt.validateDB(db)
			}
		})
	}
}

func Test_deleteExpiredMailboxes(t *testing.T) {
	Assert := assert.New(t)
	tests := []struct {
		name       string
		getDB      func() *letterboxfakes.FakeDatastore
		validateDB func(db *letterboxfakes.FakeDatastore)
		wantErr    bool
	}{
		{
			name: "Should delete the expired mailboxes",
			getDB: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				now := time.Now()
				db.GetExpiredMailboxesReturns([]Mailbox{
					{
						Name:    "foo",
						Expires: &now,
					},
				}, nil)
				return db
			},
			validateDB: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(1, db.DeleteMailboxCallCount())
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := tt.getDB()
			if err := DeleteExpiredMailboxes(db, test.Logger()); (err != nil) != tt.wantErr {
				t.Errorf("deleteExpiredMailboxes() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.validateDB != nil {
				tt.validateDB(db)
			}
		})
	}
}

func TestDisableMailboxByExternalID(t *testing.T) {
	Assert := assert.New(t)

	type args struct {
		id      string
		getter  *letterboxfakes.FakeDatastore
		updater *letterboxfakes.FakeDatastore
	}

	tests := []struct {
		name      string
		args      args
		validater func(db *letterboxfakes.FakeDatastore)
		wantErr   bool
	}{
		{
			name: "Disables a mailbox by a given external ID",
			args: args{
				id: "abcdefg",
				getter: test.DB(func(db *letterboxfakes.FakeDatastore) {
					db.GetMailboxByExternalIDReturns(test.Mailbox(), nil)
				}),
				updater: test.DB(),
			},
			validater: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(1, db.UpdateMailboxCallCount())
				m := db.UpdateMailboxArgsForCall(0)
				Assert.False(m.Enabled)
			},
			wantErr: false,
		},
		{
			name: "Errors if the specified mailbox can't be found",
			args: args{
				id: "abcdefg",
				getter: test.DB(func(db *letterboxfakes.FakeDatastore) {
					db.GetMailboxByExternalIDReturns(nil, ErrNotFound)
				}),
				updater: test.DB(),
			},
			wantErr: true,
		},
		{
			name: "Errors if there's an error during update",
			args: args{
				id: "abcdefg",
				getter: test.DB(func(db *letterboxfakes.FakeDatastore) {
					db.GetMailboxByExternalIDReturns(test.Mailbox(), nil)
				}),
				updater: test.DB(func(db *letterboxfakes.FakeDatastore) {
					db.UpdateMailboxReturns(ErrBackendError)
				}),
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DisableMailboxByExternalID(tt.args.id, tt.args.getter, tt.args.updater); (err != nil) != tt.wantErr {
				t.Errorf("DisableMailboxByExternalID() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.validater != nil {
				tt.validater(tt.args.updater)
			}
		})
	}
}
