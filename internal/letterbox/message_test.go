package letterbox_test

import (
	"testing"

	"github.com/go-test/deep"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func TestNewProxyMessage(t *testing.T) {
	type args struct {
		message *Message
	}

	tests := []struct {
		name    string
		mailbox *Mailbox
		args    args
		want    *ForwardingMessage
		wantErr bool
		dbSetup func(db *letterboxfakes.FakeDatastore)
	}{
		{
			name: "create a proxied email",
			mailbox: &Mailbox{
				Address:      "foo@example.com",
				ProxyAddress: "asdfzxcvqwerty@example.com",
				ExternalID:   "abcdefg",
			},
			args: args{
				message: &Message{
					FromHeader: "jim.jones@source.com",
					Subject:    "Hello World",
					TextBody:   "Hello",
					HTMLBody:   "<div>hello</div>",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetMailboxUserReturns(&User{Email: "foo@target.com"}, nil)
			},
			want: &ForwardingMessage{
				FromHeader: "asdfzxcvqwerty@example.com",
				ToHeader:   "foo@target.com",
				Subject:    "[proxied] Hello World",
				TextBody:   "Hello",
				HTMLBody:   "<div>http://foo.example.com abcdefg</div><div>hello</div>",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := &letterboxfakes.FakeDatastore{}
			tt.dbSetup(db)

			bb := test.BannerBuilder(
				test.WithURL("http://foo.example.com"),
				test.WithTemplate("<div>{{ .URL }} {{ .ID }}</div>"),
			)

			got, err := NewProxyMessage(tt.args.message, tt.mailbox, test.Logger(), db, bb)
			if (err != nil) != tt.wantErr {
				t.Errorf("SMTPSession.CreateProxyMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if diff := deep.Equal(got, tt.want); diff != nil {
				t.Error(diff)
			}
		})
	}
}
