package letterbox

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/jinzhu/gorm"
)

// User stores information about a user.
type User struct {
	gorm.Model `json:"-"`
	Username   string `gorm:"unique_index;type:varchar(100)" json:"username,omitempty"`
	Email      string `gorm:"type:varchar(100)" json:"email,omitempty"`
	Password   string `gorm:"type:varchar(255)" json:"password,omitempty"`
}

// Validate validates whether the values of the User are valid.
func (u *User) Validate() error {
	return validation.ValidateStruct(u,
		validation.Field(&u.Username, validation.Required, validation.Length(1, 100)),
		validation.Field(&u.Email, is.EmailFormat),
		validation.Field(&u.Password, validation.Required),
	)
}

// Scrub empties the password hash from a user object.
func (u *User) Scrub() {
	u.Password = ""
}

// UserDatastore defines all our persistence and retrieval mechanisms for a
// particular user.
type UserDatastore interface {
	AddUser(u *User) error
	GetUser(name string) (*User, error)
	UpdateUser(u *User) error
	GetAllUsers() ([]User, error)
	GetMailboxUser(mailbox *Mailbox) (*User, error)
}
