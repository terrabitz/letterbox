package letterbox

// Info contains some metadata about the server. This information is
// typically baked in at compile time, so in most development cases these fields
// will be empty.
type Info struct {
	Version string
	Commit  string
	Date    string
	BuiltBy string
}
