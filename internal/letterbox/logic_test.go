package letterbox_test

import (
	"errors"
	"testing"

	"github.com/alexedwards/argon2id"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
)

func TestAddInitialAdminUser(t *testing.T) {
	type args struct {
		user *User
	}

	tests := []struct {
		name    string
		args    args
		dbSetup func(*letterboxfakes.FakeDatastore)
		wantErr bool
	}{
		{
			name: "Creates a new user",
			args: args{
				user: &User{
					Username: "foo",
					Password: "asdf",
					Email:    "foo@example.com",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: false,
		},
		{
			name: "Fails if user validation fails",
			args: args{
				user: &User{
					Username: "foo",
					// Password: "No password provided",
					Email: "foo@example.com",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if users already exists",
			args: args{
				user: &User{
					Username: "foo",
					Password: "asdf",
					Email:    "foo@example.com",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{
					{
						Username: "bar",
						Password: "password",
						Email:    "bar@example.com",
					},
				}, nil)
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if users can't be retrieved",
			args: args{
				user: &User{
					Username: "foo",
					Password: "asdf",
					Email:    "foo@example.com",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns(nil, errors.New("could not retrieve users"))
				db.AddUserReturns(nil)
			},
			wantErr: true,
		},
		{
			name: "Fails if there's an error on save",
			args: args{
				user: &User{
					Username: "foo",
					Password: "asdf",
					Email:    "foo@example.com",
				},
			},
			dbSetup: func(db *letterboxfakes.FakeDatastore) {
				db.GetAllUsersReturns([]User{}, nil)
				db.AddUserReturns(errors.New("could not save user"))
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			db := &letterboxfakes.FakeDatastore{}
			hasher := ArgonHasher{*argon2id.DefaultParams}
			tt.dbSetup(db)
			if err := AddInitialAdminUser(db, hasher, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("AddInitialAdminUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_incrementOrInsertStat(t *testing.T) {
	Assert := assert.New(t)

	type args struct {
		mailbox *Mailbox
		domain  string
	}

	tests := []struct {
		name       string
		args       args
		dbSetup    func() *letterboxfakes.FakeDatastore
		dbValidate func(db *letterboxfakes.FakeDatastore)
		wantErr    bool
	}{
		{
			name: "Should increment an existing stats record",
			args: args{
				mailbox: &Mailbox{
					Name: "foo",
				},
				domain: "example.com",
			},
			dbSetup: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetStatReturns(&DomainStat{Domain: "example.com", Count: 10}, nil)
				return db
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(1, db.UpdateStatCallCount())
				Assert.Equal(0, db.CreateStatCallCount())
				updatedStat := db.UpdateStatArgsForCall(0)
				Assert.Equal(uint(11), updatedStat.Count)
			},
		},
		{
			name: "Should create a stats record if it doesn't exist",
			args: args{
				mailbox: &Mailbox{
					Name: "foo",
				},
				domain: "example.com",
			},
			dbSetup: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetStatReturns(nil, gorm.ErrRecordNotFound)
				return db
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(0, db.UpdateStatCallCount())
				Assert.Equal(1, db.CreateStatCallCount())
				_, createdStat := db.CreateStatArgsForCall(0)
				Assert.Equal(uint(1), createdStat.Count)
			},
		},
		{
			name: "Should error if the stats fetch error isn't a RecordNotFound error",
			args: args{
				mailbox: &Mailbox{
					Name: "foo",
				},
				domain: "example.com",
			},
			dbSetup: func() *letterboxfakes.FakeDatastore {
				db := &letterboxfakes.FakeDatastore{}
				db.GetStatReturns(nil, errors.New("connection refused"))
				return db
			},
			dbValidate: func(db *letterboxfakes.FakeDatastore) {
				Assert.Equal(0, db.UpdateStatCallCount())
				Assert.Equal(0, db.CreateStatCallCount())
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db := tt.dbSetup()
			if err := IncrementOrInsertStat(tt.args.mailbox, tt.args.domain, db); (err != nil) != tt.wantErr {
				t.Errorf("incrementOrInsertStat() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.dbValidate != nil {
				tt.dbValidate(db)
			}
		})
	}
}
