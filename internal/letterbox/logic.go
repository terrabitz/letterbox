package letterbox

import (
	"errors"
	"fmt"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/ProtonMail/gopenpgp/v2/helper"
	"github.com/jinzhu/gorm"
)

// AddInitialAdminUser checks (1) that there are no other users present, and (2)
// that the source request is from private IP space. If both are true, the given
// user is added as the initial admin account.
//
// TODO change this to use the HTTP API instead of interacting direcctly with the
// database.
func AddInitialAdminUser(db Datastore, hasher ArgonHasher, user *User) error {
	if err := user.Validate(); err != nil {
		return fmt.Errorf("%w: %v", ErrValidationError, err)
	}

	// TODO add a limit here, since we just need to prove the existence of one user
	if err := CanAddInitialAdminUser(db); err != nil {
		return err
	}

	if err := AddUser(user, hasher, db); err != nil {
		return err
	}

	return nil
}

// CanAddInitialAdminUser determines whether it's allowed to add the initial
// admin user. It is only allowed if there are no other users created.
func CanAddInitialAdminUser(db Datastore) error {
	existingUsers, err := db.GetAllUsers()
	if err != nil {
		return err
	}

	if len(existingUsers) > 0 {
		return fmt.Errorf("error while adding admin user: %w", ErrAlreadyExists)
	}

	return nil
}

// AddUser adds a user to the datastore.
func AddUser(u *User, hasher ArgonHasher, db Datastore) error {
	if err := u.Validate(); err != nil {
		return err
	}

	key, err := NewKeyPair(u.Username, u.Password)
	if err != nil {
		return err
	}

	hashed, err := hasher.Hash(u.Password)
	if err != nil {
		return err
	}

	u.Password = hashed

	if err := db.AddUser(u); err != nil {
		return err
	}

	if err := db.AddUserKey(u, key); err != nil {
		return err
	}

	return nil
}

// UpdateUser updates a user to the new values given. If the field of newValue
// is a zero value, it is ignored. If a new password is specified, the user's
// GPG key is re-encrypted under the new password.
func UpdateUser(username, oldPassword string, newValue *User, hasher ArgonHasher, db Datastore) error {
	user, err := db.GetUser(username)
	if err != nil {
		return fmt.Errorf("user '%s': %w", username, ErrNotFound)
	}

	if newValue.Password != "" {
		user.Password = newValue.Password

		var hashed string

		hashed, err = hasher.Hash(user.Password)
		if err != nil {
			return fmt.Errorf("%w: %v", ErrHashFail, err)
		}

		user.Password = hashed
	}

	if newValue.Email != "" {
		user.Email = newValue.Email
	}

	if err = user.Validate(); err != nil {
		return fmt.Errorf("%w: %v", ErrValidationError, err)
	}

	// TODO only execute the key rotation steps if a new password is specified.
	key, err := db.GetUserKey(user)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrBackendError, err)
	}

	err = UpdateKey(key, newValue.Password, oldPassword)
	if err != nil {
		return fmt.Errorf("%w: %v", ErrBackendError, err)
	}

	// TODO we NEED to be able to roll this back if we get a failure later down
	// the line. This should absolutely be done in a transaction.
	if err = db.UpdateUserKey(key); err != nil {
		return fmt.Errorf("%w: couldn't update key for user '%s': %v", ErrBackendError, username, err)
	}

	if err := db.UpdateUser(user); err != nil {
		return fmt.Errorf("%w: couldn't update user '%s': %v", ErrBackendError, username, err)
	}

	return nil
}

// UpdateUserKey updates the user key for the given user.
func UpdateUserKey(user *User, newKey *UserKey, password string, db Datastore) error {
	key, err := db.GetUserKey(user)
	if err != nil {
		return err
	}

	oldKey := &UserKey{
		PrivateKey: key.PrivateKey,
		PublicKey:  key.PublicKey,
	}

	key.PrivateKey = ""
	key.PublicKey = newKey.PublicKey

	// Validate that the public key is a valid armored key.
	if _, err := crypto.NewKeyFromArmored(key.PublicKey); err != nil {
		return err
	}

	if err := db.UpdateUserKey(key); err != nil {
		return fmt.Errorf("%w: couldn't update user key for user '%s': %v", ErrBackendError, user.Username, err)
	}

	// Rotate all the messages to be encrypted under the new key.
	if err := RotateMessageKeys(user, password, oldKey, key, db); err != nil {
		return fmt.Errorf("%w: couldn't rotate message key for user '%s': %v", ErrBackendError, user.Username, err)
	}

	return nil
}

// GetDomain retreives the domain object for a specific domain name.
func GetDomain(domain string, db Datastore) (*Domain, error) {
	d, err := db.GetDomain(domain)
	if err != nil {
		return nil, fmt.Errorf("could not find domain '%s': %w", domain, ErrNotFound)
	}

	return d, nil
}

// DeleteDomain deletes the domain with the given name.
func DeleteDomain(domain string, db Datastore) error {
	d, err := GetDomain(domain, db)
	if err != nil {
		return err
	}

	return db.DeleteDomain(d)
}

// IncrementOrInsertStat upserts a new statistic to indicate that another email
// from the given source domain has be received.
func IncrementOrInsertStat(mailbox *Mailbox, domain string, db Datastore) error {
	stat, err := db.GetStat(mailbox, domain)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			stat = &DomainStat{
				Count:  1,
				Domain: domain,
			}

			return db.CreateStat(mailbox, stat)
		}

		return fmt.Errorf("could not get domain stats: %w: %v", ErrBackendError, err)
	}

	stat.Count++

	return db.UpdateStat(stat)
}

// RotateMessageKeys rotates the keys used to encrypt all messages owned by a
// particular user. It attempts to re-encrypt every message it can; however, if
// there is a message encrypted with an unknown key, it will be left alone.
func RotateMessageKeys(user *User, password string, oldKey, newKey *UserKey, db Datastore) error {
	messages, err := db.GetMessagesForUser(user)
	if err != nil {
		return err
	}

	oldFingerprints, err := helper.GetSHA256Fingerprints(oldKey.PublicKey)
	if err != nil {
		return err
	}

	var reencrypted []EncryptedMessage //nolint:prealloc // It's unknown what the final size is

	for _, message := range messages {
		// Validate whether the message should be decryptable with the old key
		if message.Fingerprint != oldFingerprints[0] {
			continue
		}

		var unencrypted *Message

		err := DecryptObject(oldKey, password, message.Content, &unencrypted)
		if err != nil {
			return err
		}

		encrypted, err := EncryptMessage(unencrypted, newKey)
		if err != nil {
			return err
		}

		message.Content = encrypted.Content
		message.Fingerprint = encrypted.Fingerprint
		reencrypted = append(reencrypted, message)
	}

	for i := 0; i < len(reencrypted); i++ {
		if err := db.UpdateMessage(&reencrypted[i]); err != nil {
			return err
		}
	}

	return nil
}
