package letterbox

import (
	"github.com/jinzhu/gorm"
)

// DomainStat represents a count of unique sources we've seen send mail to this
// mailbox.
type DomainStat struct {
	gorm.Model `json:"-"`
	MailboxID  uint   `json:"-"`
	Domain     string `json:"domain,omitempty"`
	Count      uint   `json:"count,omitempty"`
}

// MailboxStatsDatastore defines our persistence and retrieval mechaisms for
// DomainsStat.
type MailboxStatsDatastore interface {
	GetStats(mailbox *Mailbox) ([]DomainStat, error)
	GetStat(mailbox *Mailbox, domain string) (*DomainStat, error)
	UpdateStat(stat *DomainStat) error
	CreateStat(mailbox *Mailbox, stat *DomainStat) error
}
