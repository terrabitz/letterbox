package letterbox

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/jinzhu/gorm"
)

// Domain represents an RFC1034 domain name.
type Domain struct {
	gorm.Model `json:"-"`
	Domain     string `gorm:"type:varchar(255)" json:"domain,omitempty"`
}

// Validate validates that the given domain is a valid RFC1034 domain.
func (d *Domain) Validate() error {
	return validation.ValidateStruct(d,
		validation.Field(&d.Domain, validation.Required, is.Domain),
	)
}

// DomainDatastore represents the persistence and retrieval mechanisms for
// Domain objects.
type DomainDatastore interface {
	AddDomain(d *Domain) error
	GetDomain(name string) (*Domain, error)
	GetAllDomains() ([]Domain, error)
	DeleteDomain(d *Domain) error
}
