package letterbox

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/ProtonMail/gopenpgp/v2/helper"
	"github.com/alexedwards/argon2id"
)

// NewKeyPair generates a new PGP key for the given user.
// The password in the user object MUST be unhashed.
func NewKeyPair(username, password string) (*UserKey, error) {
	email := fmt.Sprintf("%s@example.com", username)

	privateKey, err := crypto.GenerateKey(username, email, "x25519", 0)
	if err != nil {
		return nil, err
	}

	// TODO check if the password is hashed
	lockedPrivateKey, err := privateKey.Lock([]byte(password))
	if err != nil {
		return nil, err
	}

	publicKeyASCII, err := lockedPrivateKey.GetArmoredPublicKey()
	if err != nil {
		return nil, err
	}

	privateKeyASCII, err := lockedPrivateKey.Armor()
	if err != nil {
		return nil, err
	}

	return &UserKey{
		PublicKey:  publicKeyASCII,
		PrivateKey: privateKeyASCII,
	}, nil
}

// EncryptObject serializes a given object to JSON, then encrypts the string.
func EncryptObject(key *UserKey, v interface{}) (string, error) {
	j, err := json.Marshal(v)
	if err != nil {
		return "", err
	}

	return helper.EncryptMessageArmored(key.PublicKey, string(j))
}

// DecryptObject decrypts an encrypted message using the given key and
// password, and marshals it into the interface{} provided in v.
func DecryptObject(key *UserKey, password, ciphertext string, v interface{}) error {
	s, err := helper.DecryptMessageArmored(key.PrivateKey, []byte(password), ciphertext)
	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(s), v)
	if err != nil {
		return err
	}

	return nil
}

// UpdateKey rotates a user key's passphrase (if a private key is stored on the
// server).
func UpdateKey(key *UserKey, newPassword, oldPassword string) error {
	if key.PrivateKey != "" {
		newPrivateKey, err := helper.UpdatePrivateKeyPassphrase(key.PrivateKey, []byte(oldPassword), []byte(newPassword))
		if err != nil {
			return err
		}

		key.PrivateKey = newPrivateKey
	}

	return nil
}

// EncryptMessage encrypts an email message under the given key.
func EncryptMessage(message *Message, key *UserKey) (*EncryptedMessage, error) {
	encrypted, err := EncryptObject(key, message)
	if err != nil {
		return nil, err
	}

	fingerprint, err := helper.GetSHA256Fingerprints(key.PublicKey)
	if err != nil {
		return nil, err
	}

	return &EncryptedMessage{
		Content:     encrypted,
		Fingerprint: fingerprint[0], // only use the primary public key fingerprint
	}, nil
}

// GetRandomString returns a random alphanumeric string of length n.
func GetRandomString(n int) (string, error) {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz0123456789")

	b := make([]rune, n)
	for i := range b {
		letter, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterRunes))))
		if err != nil {
			return "", err
		}

		b[i] = letterRunes[letter.Int64()]
	}

	return string(b), nil
}

type ArgonHasher struct {
	Params argon2id.Params
}

func (a *ArgonHasher) Hash(password string) (string, error) {
	return argon2id.CreateHash(password, &a.Params)
}

func (a *ArgonHasher) Validate(password, hash string) (bool, error) {
	return argon2id.ComparePasswordAndHash(password, hash)
}
