package letterbox

import (
	"fmt"
	"strings"

	"golang.org/x/net/publicsuffix"
)

// StrictDomainCheck checks whether the domains are exactly equivalent. This is
// normally less useful, since there are many cases where a sending domain will
// be slighlly differenct than the domain in the FROM header.
type StrictDomainCheck struct{}

func (*StrictDomainCheck) ValidateDomainAlignment(a, b string) error {
	cleanedA := strings.ToLower(strings.Trim(a, "."))
	cleanedB := strings.ToLower(strings.Trim(b, "."))

	if cleanedA != cleanedB {
		return fmt.Errorf("%w: '%s' and '%s'", ErrDomainMisalignment, cleanedA, cleanedB)
	}

	return nil
}

// FlexibleDomainCheck checks whether the given domains fall within the same
// effective top-level-domain + 1. Is uses the public suffix list to determine
// whether the given domains are organizationally equivalent.
type FlexibleDomainCheck struct{}

func (*FlexibleDomainCheck) ValidateDomainAlignment(a, b string) error {
	cleanedA := strings.ToLower(strings.Trim(a, "."))
	cleanedB := strings.ToLower(strings.Trim(b, "."))

	orgDomainA, err := publicsuffix.EffectiveTLDPlusOne(cleanedA)
	if err != nil {
		return err
	}

	orgDomainB, err := publicsuffix.EffectiveTLDPlusOne(cleanedB)
	if err != nil {
		return err
	}

	if orgDomainA != orgDomainB {
		return fmt.Errorf("%w: '%s' and '%s'", ErrDomainMisalignment, orgDomainA, orgDomainB)
	}

	return nil
}
