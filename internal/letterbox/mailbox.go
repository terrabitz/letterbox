package letterbox

import (
	"fmt"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"

	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

// MailboxDatastore defines the persistence and retrieval mechanisms for
// mailboxes.
type MailboxDatastore interface {
	MailboxExternalIDGetter
	MailboxUpdater
	AddMailbox(d *Domain, u *User, m *Mailbox) error
	GetMailboxesByDomain(domain *Domain) ([]Mailbox, error)
	GetMailboxByDomain(domain *Domain, name string) (*Mailbox, error)
	GetAllMailboxes() ([]Mailbox, error)
	GetExpiredMailboxes() ([]Mailbox, error)
	GetCatchAll(d *Domain) (*Mailbox, error)
	DeleteMailbox(m *Mailbox) error
}

type MailboxUpdater interface {
	UpdateMailbox(m *Mailbox) error
}

type MailboxExternalIDGetter interface {
	GetMailboxByExternalID(id string) (*Mailbox, error)
}

// Mailbox represents all the relevant information about a particular mailbox.
type Mailbox struct {
	gorm.Model `json:"-"`
	// DomainID is the reference to the domain of this address
	DomainID uint `json:"-"`

	// UserID is the reference to the user that owns this mailbox
	UserID uint `json:"-"`

	// Name is the name of this mailbox. This is the first part of the associated email address
	Name string `gorm:"type:varchar(100)" json:"name,omitempty"`

	// Address holds the full email address of this mailbox
	Address string `json:"address,omitempty"`

	// ProxyAddress is the return address used in forwarded emails
	ProxyAddress string `json:"-"`

	// Enabled indicates whether this mailbox is enabled or not. If not, then
	// all mails for this address are rejected similarly to if this mailbox
	// didn't exist.
	Enabled bool `json:"enabled"`

	// IsCatchAll indicates if this is a catch-all mailbox. If it is, all mail
	// to this mailbox's domain will go to this mailbox if it isn't matched to
	// another specific mailbox. There can only be one catch-all mailbox per
	// domain.
	IsCatchAll bool `json:"isCatchAll,omitempty"`

	// Expires indicates an absolute (RFC3339) time for this mailbox to expire. If this is set,
	// the mailbox will be deleted after the designated time.
	Expires *time.Time `json:"expires,omitempty"`

	// DomainWhitelist contains an array of domains to match against. If this is
	// set, the "from" header for all mail destined for this mailbox must match
	// one of the domains listed in this whitelist. Whitelist domains may also
	// be wildcards, with '*' matching multiple characters and '?' matching
	// single characters.
	DomainWhitelist pq.StringArray `gorm:"type:varchar(100)[]" json:"domainWhitelist,omitempty"`

	// ExternalID is a randomly-generated ID which can be used to reference this
	// mailbox from unauthenticated contexts, such as the "disable mailbox"
	// endpoint.
	ExternalID string `json:"-"`
}

// Validate validates whether this mailbox contains valid values.
func (m *Mailbox) Validate() error {
	return validation.ValidateStruct(m,
		validation.Field(&m.Name, validation.Required),
	)
}

// GetMailbox gets the mailbox with the given domain and name.
func GetMailbox(domain, name string, db Datastore) (*Mailbox, error) {
	d, err := GetDomain(domain, db)
	if err != nil {
		return nil, err
	}

	m, err := db.GetMailboxByDomain(d, name)
	if err != nil {
		return nil, fmt.Errorf("mailbox '%s' in domain '%s': %w", name, domain, ErrNotFound)
	}

	return m, nil
}

// AddMailbox adds a new mailbox to a specific domain, and attaches it to a
// specific user.
func AddMailbox(domain string, user *User, mailbox *Mailbox, db Datastore) error {
	const proxyAddressLength int = 20

	const externalIDLength int = 30

	if err := mailbox.Validate(); err != nil {
		return err
	}

	d, err := GetDomain(domain, db)
	if err != nil {
		return err
	}

	if _, err = db.GetMailboxByDomain(d, mailbox.Name); err == nil {
		return fmt.Errorf("error adding mailbox '%s' to domain '%s': %w", mailbox.Name, d.Domain, ErrAlreadyExists)
	}

	mailbox.Address = fmt.Sprintf("%s@%s", mailbox.Name, d.Domain)
	mailbox.Enabled = true

	proxyAddress, err := GetRandomString(proxyAddressLength)
	if err != nil {
		return err
	}

	mailbox.ProxyAddress = fmt.Sprintf("%s@%s", proxyAddress, d.Domain)

	mailbox.ExternalID, err = GetRandomString(externalIDLength)
	if err != nil {
		return err
	}

	if err := db.AddMailbox(d, user, mailbox); err != nil {
		return err
	}

	return nil
}

// UpdateMailbox updates the values of an existing mailbox.
func UpdateMailbox(domain, name string, newValue *Mailbox, db Datastore) error {
	mailbox, err := GetMailbox(domain, name, db)
	if err != nil {
		return err
	}

	mailbox.Enabled = newValue.Enabled

	// TODO Bools like IsCatchAll should be a pointer so we can catch when the
	// value is specified vs. when it's the default zero value.
	if newValue.IsCatchAll != mailbox.IsCatchAll && newValue.IsCatchAll {
		d, err := GetDomain(domain, db)
		if err != nil {
			return err
		}

		if existing, err := db.GetCatchAll(d); err == nil {
			return fmt.Errorf("error while update mailbox '%s' as the catchall: %w", existing.Address, ErrAlreadyExists)
		}
	}

	mailbox.IsCatchAll = newValue.IsCatchAll

	if newValue.Expires != nil {
		if newValue.Expires.Before(time.Now()) {
			return fmt.Errorf("%w: expiry time must come after current time", ErrValidationError)
		}

		expiry := newValue.Expires.Round(time.Second)
		mailbox.Expires = &expiry
	}

	if newValue.DomainWhitelist != nil {
		mailbox.DomainWhitelist = newValue.DomainWhitelist
	}

	if err := db.UpdateMailbox(mailbox); err != nil {
		return err
	}

	return nil
}

// DeleteMailbox deletes the mailbox with the given name and domain.
func DeleteMailbox(domain, name string, db Datastore) error {
	mailbox, err := GetMailbox(domain, name, db)
	if err != nil {
		return err
	}

	return db.DeleteMailbox(mailbox)
}

// DeleteExpiredMailboxes deletes all mailboxes where the current time is past
// their expiry time.
func DeleteExpiredMailboxes(db Datastore, log logger.Logger) error {
	expiredMailboxes, err := db.GetExpiredMailboxes()
	if err != nil {
		return err
	}

	for i := 0; i < len(expiredMailboxes); i++ {
		mailbox := &expiredMailboxes[i]

		log.Infow("cleaning up mailbox",
			logger.Fields{
				"address": mailbox.Address,
			},
		)

		if err := db.DeleteMailbox(mailbox); err != nil {
			return err
		}
	}

	return nil
}

// DisableMailboxByExternalID disables a particular mailbox given its external
// identifier. This may be called from unauthenticated contexts.
func DisableMailboxByExternalID(id string, getter MailboxExternalIDGetter, updater MailboxUpdater) error {
	m, err := getter.GetMailboxByExternalID(id)
	if err != nil {
		return fmt.Errorf("error searching for mailbox with external address '%s': %w", id, err)
	}

	m.Enabled = false
	if err := updater.UpdateMailbox(m); err != nil {
		return fmt.Errorf("error while disabling mailbox with external ID '%s': %w", id, err)
	}

	return nil
}

// GetMailboxForAddress retreives a Mailbox object given an RFC822-formated
// email address.
func GetMailboxForAddress(to string, domainDB DomainDatastore, mailboxDB MailboxDatastore) (*Mailbox, error) {
	toAddr, err := SplitAddr(to)

	if err != nil {
		return nil, fmt.Errorf("%w: couldn't parse address '%s", ErrInvalidDestination, to)
	}

	domain, err := domainDB.GetDomain(toAddr.Domain)
	if err != nil {
		return nil, fmt.Errorf("%w: no domain found matching '%s'", ErrInvalidDestination, toAddr.Domain)
	}

	mailbox, err := mailboxDB.GetMailboxByDomain(domain, toAddr.Username)
	if err != nil {
		mailbox, err = mailboxDB.GetCatchAll(domain)
	}

	if err != nil || !mailbox.Enabled {
		return nil, fmt.Errorf("%w: no username found matching '%s'", ErrInvalidDestination, toAddr.Username)
	}

	return mailbox, nil
}
