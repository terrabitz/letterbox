package letterbox

import (
	"fmt"
	"net/mail"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
)

// EncryptedMessage represents a Message that has been encrypted using a GPG
// UserKey.
type EncryptedMessage struct {
	gorm.Model `json:"-"`

	// MailboxID contains the parent mailbox this message is attached to.
	MailboxID uint `json:"-"`

	// Content holds an object of type MailMessage after it's been
	// encrypted.
	Content string `json:"encrypted_message"`

	// Fingerprint holds the SHA256 fingerprint of the public key that this
	// message was encrypted with
	Fingerprint string `json:"fingerprint"`
}

// Message defines all the relevant fields of an email message.
type Message struct {
	From          string    `json:"from,omitempty"`
	To            string    `json:"to,omitempty"`
	HTMLBody      string    `json:"html_body,omitempty"`
	TextBody      string    `json:"text_body,omitempty"`
	Raw           string    `json:"raw,omitempty"`
	ReceivedTime  time.Time `json:"received_time,omitempty"`
	FromHeader    string    `json:"from_header,omitempty"`
	ToHeader      string    `json:"to_header,omitempty"`
	DateHeader    time.Time `json:"date_header,omitempty"`
	Subject       string    `json:"subject,omitempty"`
	DKIMStatus    string    `json:"dkim_status,omitempty"`
	SPFResult     string    `json:"spf_result,omitempty"`
	DKIMAlignment string    `json:"dkim_alignment,omitempty"`
	SPFAlignment  string    `json:"spf_alignment,omitempty"`
}

// MessageDatastore defines our persistence and retrieval mechaisms for messages.
type MessageDatastore interface {
	AddMessage(mb *Mailbox, m *EncryptedMessage) error
	GetMessages(mb *Mailbox) ([]EncryptedMessage, error)
	UpdateMessage(m *EncryptedMessage) error
	DeleteMessage(m *EncryptedMessage) error
	GetMessagesForUser(u *User) ([]EncryptedMessage, error)
}

// AllMessages is a struct to bind together both encrypted and decrypted
// messages.
type AllMessages struct {
	Messages          []Message          `json:"messages,omitempty"`
	EncryptedMessages []EncryptedMessage `json:"encrypted_messages,omitempty"`
}

// GetMessages returns all messages for a user and attempts to transparently
// decrypt them (if they're encrypted with a private key stored in the server).
func GetMessages(mailbox *Mailbox, user *User, password string, db Datastore) (*AllMessages, error) {
	key, err := db.GetUserKey(user)
	if err != nil {
		return nil, err
	}

	messages, err := db.GetMessages(mailbox)
	if err != nil {
		return nil, err
	}

	allMessages := &AllMessages{}

	for _, message := range messages {
		m := Message{}

		err := DecryptObject(key, password, message.Content, &m)
		if err == nil {
			allMessages.Messages = append(allMessages.Messages, m)
		} else {
			// TODO we should eventually keep the output in the same array so
			// clients can easily do things like pagination or limits
			allMessages.EncryptedMessages = append(allMessages.EncryptedMessages, message)
		}
	}

	return allMessages, nil
}

func AddMessage(
	message *Message,
	mailbox *Mailbox,
	log logger.Logger,
	userDB UserDatastore,
	keyDB UserKeyDatastore,
	messageDB MessageDatastore,
) error {
	log = log.With(
		logger.Fields{
			"mailbox": mailbox.Address,
			"address": message.ToHeader,
		},
	)

	user, err := userDB.GetMailboxUser(mailbox)
	if err != nil {
		log.Infow("no user attached")

		return ErrBackendError
	}

	key, err := keyDB.GetUserKey(user)
	if err != nil {
		log.Infow("no user key attached to user",
			logger.Fields{
				"user": user.Username,
			},
		)

		return ErrBackendError
	}

	encrypted, err := EncryptMessage(message, key)
	if err != nil {
		return err
	}

	if err := messageDB.AddMessage(mailbox, encrypted); err != nil {
		return err
	}

	return nil
}

// Address contains information about a parsed email address.
type Address struct {
	// A human-readable name for the address
	Name string
	// The full email address (e.g. "foo@example.com")
	Full string
	// The username portion of the email address (e.g. "foo")
	Username string
	// The domain portion of the email address (e.g. "example.com")
	Domain string
}

// SplitAddr parses a given RFC822 email address into its component parts.
func SplitAddr(address string) (*Address, error) {
	const maxAddressSplits = 2

	a, err := mail.ParseAddress(address)
	if err != nil {
		return nil, err
	}

	split := strings.Split(a.Address, "@")
	if len(split) != maxAddressSplits {
		return nil, fmt.Errorf("%w: '%s' not a valid email address", ErrParsingError, address)
	}

	return &Address{
		Name:     a.Name,
		Full:     a.Address,
		Username: split[0],
		Domain:   split[1],
	}, nil
}

// ForwardingMessage is a stripped-down version of Message that contains only
// enough information for an SMTP client.
type ForwardingMessage struct {
	FromHeader string
	ToHeader   string
	Subject    string
	TextBody   string
	HTMLBody   string
}

// NewProxyMessage creates a new message to be sent via an SMTP client. The
// proxy email sets the "To" header as the mailbox user's configured email. If
// no forwarding email configuration is found, then both returned items are nil.
func NewProxyMessage(
	message *Message,
	mailbox *Mailbox,
	log logger.Logger,
	userDB UserDatastore,
	bb *banner.Builder,
) (*ForwardingMessage, error) {
	log = log.With(
		logger.Fields{
			"mailbox": mailbox.Address,
		},
	)

	proxyAddress := mailbox.ProxyAddress
	if proxyAddress == "" {
		log.Infow("no proxy address configured")

		return nil, fmt.Errorf("%w: no proxy email associated with mailbox '%s'", ErrValidationError, mailbox.Address)
	}

	if _, err := mail.ParseAddress(proxyAddress); err != nil {
		// TODO make structured errors that are returnable, so they can be used by
		// zap on the calling side rather than the callee side
		log.Infow("invalid proxy address",
			logger.Fields{
				"address": proxyAddress,
			},
		)

		return nil, fmt.Errorf("%w: invalid proxy address '%s'", ErrValidationError, proxyAddress)
	}

	user, err := userDB.GetMailboxUser(mailbox)
	if err != nil {
		return nil, fmt.Errorf("%w: couldn't find user associated with mailbox '%s'", ErrValidationError, mailbox.Name)
	}

	log = log.With(
		logger.Fields{
			"user":    user.Username,
			"address": user.Email,
		},
	)

	if user.Email == "" {
		log.Infow("user has no forward email configured; skipping email forward")

		return nil, nil
	}

	if _, err = mail.ParseAddress(user.Email); err != nil {
		log.Infow("user has invalid forward email")

		return nil, fmt.Errorf("couldn't parse forwarding email '%s': %w", user.Email, err)
	}

	htmlBody := message.HTMLBody

	b, err := bb.Make(mailbox.ExternalID)
	if err != nil {
		// Treat this error as non-terminating, since the extra banner isn't
		// critical to our message forwarding.
		log.InfoErr("error making HTML banner", err)
	} else {
		htmlBody = b + htmlBody
	}

	outboundMessage := &ForwardingMessage{
		FromHeader: proxyAddress,
		ToHeader:   user.Email,
		Subject:    fmt.Sprintf("[proxied] %s", message.Subject),
		TextBody:   message.TextBody,
		HTMLBody:   htmlBody,
	}

	return outboundMessage, nil
}
