package letterbox_test

import (
	"testing"

	"github.com/ProtonMail/gopenpgp/v2/crypto"
	"github.com/stretchr/testify/assert"

	. "gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/test"
)

func Test_newKeyPair(t *testing.T) {
	t.Parallel()
	Assert := assert.New(t)

	type args struct {
		user *User
	}

	tests := []struct {
		name     string
		args     args
		validate func(*UserKey)
		wantErr  bool
	}{
		{
			name: "Generate an openpgp keypair",
			args: args{
				user: &User{
					Username: "foo",
					Password: "P@ssw0rd",
				},
			},
			validate: func(keypair *UserKey) {
				privateKey, err := crypto.NewKeyFromArmored(keypair.PrivateKey)
				Assert.Nil(err)

				Assert.True(privateKey.IsPrivate())

				_, err = privateKey.Unlock([]byte("P@ssw0rd"))
				Assert.Nil(err)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewKeyPair(tt.args.user.Username, tt.args.user.Password)
			if (err != nil) != tt.wantErr {
				t.Errorf("newKeyPair() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			tt.validate(got)
		})
	}
}

func Benchmark_newKeyPair(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = NewKeyPair("foo", "P@ssw0rd")
	}
}

func Test_encrypt_decrypt(t *testing.T) {
	Assert := assert.New(t)

	type TestStruct struct {
		S string
		I int
	}

	testKey, err := test.GetTestKey()
	if err != nil {
		t.Fatalf("%s", err)
	}

	plain := &TestStruct{
		S: "Hello world",
		I: 42,
	}

	ciphertext, err := EncryptObject(testKey, plain)
	Assert.Nil(err)
	Assert.NotNil(ciphertext)

	decrypted := &TestStruct{}
	err = DecryptObject(testKey, "P@ssw0rd", ciphertext, decrypted)
	Assert.Nil(err)

	Assert.Equal(plain, decrypted)
}

func Benchmark_GetRandomString(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, _ = GetRandomString(10)
	}
}
