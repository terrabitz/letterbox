# Test Keys

The keys in this directory are for performing unit tests on the openpgp functions.

* **test.pem** - The private x25519 key
* **test_pub.pem** - The public x25519 key

The password for the private test key is `P@ssw0rd`
