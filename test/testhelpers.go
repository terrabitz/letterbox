package test

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path"
	"runtime"

	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

var (
	ErrPathDiscovery = errors.New("could not identify test objects path")
)

func GetTestKey() (*letterbox.UserKey, error) {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil, ErrPathDiscovery
	}

	testKeyDir := path.Join(path.Dir(filename), "key")
	pubKeyFile := path.Join(testKeyDir, "test_pub.pem")
	privKeyFile := path.Join(testKeyDir, "test.pem")

	pubKey, err := ioutil.ReadFile(pubKeyFile)
	if err != nil {
		return nil, err
	}

	privKey, err := ioutil.ReadFile(privKeyFile)
	if err != nil {
		return nil, err
	}

	return &letterbox.UserKey{
		PrivateKey: string(privKey),
		PublicKey:  string(pubKey),
	}, nil
}

func GetTestMessage(name string) io.Reader {
	_, currentFile, _, ok := runtime.Caller(0)
	if !ok {
		panic("could not identify test objects path")
	}

	filename := path.Join(path.Dir(currentFile), "example_emails", name+".txt")

	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}

	return f
}
