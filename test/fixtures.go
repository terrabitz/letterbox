package test

import (
	"io/ioutil"
	"testing"

	"gitlab.com/hackandsla.sh/letterbox/internal/banner"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox/letterboxfakes"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger"
	"gitlab.com/hackandsla.sh/letterbox/internal/logger/loggerfakes"
)

type MailboxOption func(*letterbox.Mailbox)

func WithDomainAllowlist(domains ...string) MailboxOption {
	return func(m *letterbox.Mailbox) {
		m.DomainWhitelist = domains
	}
}

func Mailbox(options ...MailboxOption) *letterbox.Mailbox {
	m := &letterbox.Mailbox{
		Enabled:      true,
		Name:         "foo",
		ProxyAddress: "abcd1234@example.com",
		Address:      "foo@example.com",
	}

	for _, option := range options {
		if option != nil {
			option(m)
		}
	}

	return m
}

type BannerBuilderOption func(*banner.Config)

func WithURL(url string) BannerBuilderOption {
	return func(bc *banner.Config) {
		bc.URL = url
	}
}

func WithTemplate(template string) BannerBuilderOption {
	return func(bc *banner.Config) {
		bc.Template = template
	}
}

func BannerBuilder(options ...BannerBuilderOption) *banner.Builder {
	bc := &banner.Config{
		URL:      "https://foo.example.com",
		Template: "{{ .Hostname }}/{{ .ID }}",
	}

	for _, option := range options {
		option(bc)
	}

	bb, err := banner.NewBuilder(bc)
	if err != nil {
		panic(err)
	}

	return bb
}

func Logger() *loggerfakes.FakeLogger {
	l := &loggerfakes.FakeLogger{}
	l.WithStub = func(fields logger.Fields) logger.Logger {
		return Logger()
	}

	return l
}

type DBOption func(db *letterboxfakes.FakeDatastore)

func DB(options ...DBOption) *letterboxfakes.FakeDatastore {
	db := &letterboxfakes.FakeDatastore{}

	testKey, err := GetTestKey()
	if err != nil {
		panic(err)
	}

	db.GetStatReturns(&letterbox.DomainStat{Domain: "example.com", Count: 1}, nil)
	db.GetUserKeyReturns(testKey, nil)
	db.AddMessageReturns(nil)

	domain := &letterbox.Domain{Domain: "example.com"}
	db.GetDomainReturns(domain, nil)

	mailbox := Mailbox()
	db.GetMailboxByDomainReturns(mailbox, nil)

	user := &letterbox.User{
		Email:    "foo@foo.example.com",
		Username: "letterbox",
		Password: "$argon2id$v=19$m=65536,t=3,p=2$7+/VG2i3z1MecWI92qwy6Q$Oz9PH3XoQ2ilMvUEURkvzwer8wQnlK4Y1/a5f4PKxEg",
	}
	db.GetMailboxUserReturns(user, nil)

	for _, option := range options {
		if option != nil {
			option(db)
		}
	}

	return db
}

func Fixture(path string, t *testing.T) string {
	t.Helper()

	b, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("couldn't read file '%s': %v", path, err)
	}

	return string(b)
}
