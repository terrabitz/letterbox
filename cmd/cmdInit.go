package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"syscall"

	"github.com/alexedwards/argon2id"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh/terminal"

	"gitlab.com/hackandsla.sh/letterbox/internal/db"
	"gitlab.com/hackandsla.sh/letterbox/internal/letterbox"
)

func newCmdInit() *cobra.Command {
	return &cobra.Command{
		Use:   "init",
		Short: "Initialize the initial admin user",
		Long: "Initialize the initial admin user. " +
			"This method of admin initialization allows you to not expose " +
			"credentials over a network or leave them hanging around as environment variables",
		Run: func(cmd *cobra.Command, args []string) {
			initializeAdmin()
		},
	}
}

func initializeAdmin() {
	// TODO update this to be a simple POST request
	config := getConfig()
	sqlDB := &db.SQLDatastore{
		Config: *config.DB,
	}

	hasher := letterbox.ArgonHasher{Params: *argon2id.DefaultParams}

	if err := sqlDB.Start(); err != nil {
		fmt.Printf("could not initialize database connection: %v\n", err)
		return
	}

	// Use this as a fast fail test.
	if err := letterbox.CanAddInitialAdminUser(sqlDB); err != nil {
		fmt.Printf("can't add admin user: %v", err)
		return
	}

	username := config.Init.Username
	password := config.Init.Password
	reader := bufio.NewReader(os.Stdin)

	if username == "" {
		for {
			var err error

			fmt.Print("Enter Username: ")

			username, err = reader.ReadString('\n')
			if err != nil {
				fmt.Printf("could not read username: %v\n", err)
				continue
			}

			username = strings.TrimSpace(username)
			if username != "" {
				break
			}
		}
	}

	if password == "" {
		for {
			fmt.Print("Enter Password: ")
			//nolint:unconvert // Required for Windows (https://github.com/golang/go/issues/12978)
			bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
			if err != nil {
				fmt.Printf("could not read password: %v\n", err)
				continue
			}

			password = strings.TrimSpace(string(bytePassword))
			if password != "" {
				break
			}
		}
	}

	fmt.Println()

	err := letterbox.AddInitialAdminUser(
		sqlDB,
		hasher,
		&letterbox.User{
			Username: username,
			Password: password,
		},
	)
	if err != nil {
		fmt.Printf("could not add initial admin user: %v\n", err)
		return
	}

	fmt.Printf("successfully added user '%s'\n", username)
}
