package cmd

import (
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
)

func newCmdGenCompletion(rootCmd *cobra.Command) *cobra.Command {
	var completionValidArgs = []string{"zsh", "bash", "powershell"}

	return &cobra.Command{
		Use:       "completion",
		Short:     "Generate shell completions",
		Long:      fmt.Sprintf("Generate shell completions. Must be one of the following: [%s]", strings.Join(completionValidArgs, ", ")),
		Args:      cobra.OnlyValidArgs,
		ValidArgs: completionValidArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) != 1 {
				_ = cmd.Help()
				fmt.Printf("\n\nMust specify one of: [%s]", strings.Join(cmd.ValidArgs, ", "))
				return
			}
			name := rootCmd.Name()
			var err error
			switch args[0] {
			case "zsh":
				err = rootCmd.GenZshCompletionFile("_" + name)
			case "bash":
				err = rootCmd.GenBashCompletionFile(name + "-completion.sh")
			case "powershell":
				err = rootCmd.GenPowerShellCompletionFile(name + "-completion.ps1")
			}
			if err != nil {
				log.Fatalf("Could not generate %s completion: %s", args[0], err.Error())
			}
		},
	}
}
