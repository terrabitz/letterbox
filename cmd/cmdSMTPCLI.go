package cmd

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"

	smtp "github.com/emersion/go-smtp"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/spf13/cobra"

	"gitlab.com/hackandsla.sh/letterbox/internal/validate"
)

func newCmdSMTPCLI() *cobra.Command {
	var config *SMTPCLIConfig

	cmd := &cobra.Command{
		Use:   "smtp-cli",
		Short: "Send a SMTP message",
		Run: func(cmd *cobra.Command, args []string) {
			if err := config.Validate(); err != nil {
				log.Fatalf("Invalid values: %v", err)
			}

			messageData, err := ioutil.ReadFile(config.Message)
			checkErr(err)

			// Connect to the remote SMTP server.
			var c *smtp.Client

			tlsConfig := &tls.Config{
				//nolint:gosec // This is configurable and disabled by default
				InsecureSkipVerify: config.Insecure,
			}
			if config.DisableTLS {
				c, err = smtp.Dial(config.Server)
				checkErr(err)
			} else if config.STARTTLS {
				c, err = smtp.Dial(config.Server)
				checkErr(err)

				err = c.StartTLS(tlsConfig)
				checkErr(err)
			} else {
				c, err = smtp.DialTLS(config.Server, tlsConfig)
				checkErr(err)
			}

			// Set the sender and recipient first
			err = c.Mail(config.From, nil)
			checkErr(err)

			err = c.Rcpt(config.To)
			checkErr(err)

			// Send the email body.
			wc, err := c.Data()
			checkErr(err)

			_, err = fmt.Fprint(wc, string(messageData))
			checkErr(err)

			err = wc.Close()
			checkErr(err)

			// Send the QUIT command and close the connection.
			err = c.Quit()
			checkErr(err)
		},
	}

	config = SMTPCLIFlags(cmd)

	return cmd
}

func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

type SMTPCLIConfig struct {
	From       string `json:"from,omitempty"`
	To         string `json:"to,omitempty"`
	Message    string `json:"message,omitempty"`
	Server     string `json:"server,omitempty"`
	DisableTLS bool   `json:"disable_tls,omitempty"`
	STARTTLS   bool   `json:"starttls,omitempty"`
	Insecure   bool   `json:"insecure,omitempty"`
}

func (c *SMTPCLIConfig) Validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.From, validation.Required, is.EmailFormat),
		validation.Field(&c.To, validation.Required, is.EmailFormat),
		validation.Field(&c.Message, validation.Required, validation.By(validate.FileExists)),
		validation.Field(&c.Server, validation.Required, is.DialString),
	)
}

func SMTPCLIFlags(cmd *cobra.Command) *SMTPCLIConfig {
	config := &SMTPCLIConfig{}

	cmd.Flags().StringVarP(&config.From, "from", "f", "", "The 'From' address")
	_ = cmd.MarkFlagRequired("from")

	cmd.Flags().StringVarP(&config.To, "to", "t", "", "The 'To' address")
	_ = cmd.MarkFlagRequired("to")

	cmd.Flags().StringVarP(&config.Message, "message", "m", "", "The text file containing the message to send")
	_ = cmd.MarkFlagRequired("message")

	cmd.Flags().StringVarP(&config.Server, "server", "s", "", "The SMTP server to use")
	_ = cmd.MarkFlagRequired("server")

	cmd.Flags().BoolVar(&config.DisableTLS, "disable-tls", false, "If specified, will not use TLS")

	cmd.Flags().BoolVar(&config.STARTTLS, "starttls", false, "If specified, will use opportunistic STARTTLS rather than implicit TLS")

	cmd.Flags().BoolVarP(&config.Insecure, "insecure", "k", false, "If specified, will skip TLS verification")

	return config
}
